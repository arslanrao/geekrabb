# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181215184216) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "activities", force: :cascade do |t|
    t.string   "name"
    t.text     "detail"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "user_id"
    t.boolean  "status",                   default: true
    t.datetime "starting_time"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.text     "location"
    t.string   "gender",        limit: 10
    t.text     "age_range",                                            array: true
    t.index ["user_id"], name: "index_activities_on_user_id", using: :btree
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "attendees", force: :cascade do |t|
    t.integer  "activity_id"
    t.integer  "user_id"
    t.boolean  "going",       default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["activity_id"], name: "index_attendees_on_activity_id", using: :btree
    t.index ["user_id"], name: "index_attendees_on_user_id", using: :btree
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "subject"
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.text     "recent_message"
    t.index ["recipient_id"], name: "index_conversations_on_recipient_id", using: :btree
    t.index ["sender_id"], name: "index_conversations_on_sender_id", using: :btree
  end

  create_table "degrees", force: :cascade do |t|
    t.string   "name"
    t.string   "institution"
    t.string   "other_institute"
    t.integer  "user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["user_id"], name: "index_degrees_on_user_id", using: :btree
  end

  create_table "feedbacks", force: :cascade do |t|
    t.float    "rating"
    t.text     "comment"
    t.integer  "user_id"
    t.integer  "rated_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follows", force: :cascade do |t|
    t.string   "follower_type"
    t.integer  "follower_id"
    t.string   "followable_type"
    t.integer  "followable_id"
    t.datetime "created_at"
    t.index ["followable_id", "followable_type"], name: "fk_followables", using: :btree
    t.index ["follower_id", "follower_type"], name: "fk_follows", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "read",            default: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
    t.index ["user_id"], name: "index_messages_on_user_id", using: :btree
  end

  create_table "payments", force: :cascade do |t|
    t.string   "transaction_id"
    t.integer  "user_id"
    t.string   "response_code"
    t.string   "response_message"
    t.string   "transaction_amount"
    t.string   "transaction_currency"
    t.string   "last_4_digits"
    t.string   "first_4_digits"
    t.string   "card_brand"
    t.string   "trans_date"
    t.string   "secure_sign"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "paymentToken"
    t.string   "payerID"
    t.string   "paymentID"
    t.string   "intent"
    t.string   "returnUrl"
    t.string   "payment_type"
    t.string   "state"
    t.boolean  "mobile_payment",       default: false
    t.index ["user_id"], name: "index_payments_on_user_id", using: :btree
  end

  create_table "photos", force: :cascade do |t|
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "profile_views", force: :cascade do |t|
    t.integer  "user_viewed_id"
    t.integer  "user_id"
    t.integer  "count",          default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "user_identities", force: :cascade do |t|
    t.string   "device_id"
    t.string   "access_token"
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "device_token"
    t.string   "device_type",  limit: 20
    t.index ["access_token"], name: "index_user_identities_on_access_token", unique: true, using: :btree
    t.index ["device_id", "user_id"], name: "index_user_identities_on_device_id_and_user_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_identities_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "account_type"
    t.string   "gender"
    t.string   "city"
    t.string   "education_level"
    t.string   "institution"
    t.string   "major"
    t.datetime "availablity_start_time"
    t.datetime "availablity_end_time"
    t.float    "hourly_rate"
    t.boolean  "status",                         default: true
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "email",                          default: "",    null: false
    t.string   "encrypted_password",             default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                  default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "other_institute"
    t.string   "nationality"
    t.float    "work_experience"
    t.string   "job"
    t.string   "teaching_interests",             default: [],                 array: true
    t.string   "languages",                      default: [],                 array: true
    t.string   "teaching_modules",               default: [],                 array: true
    t.string   "hobbies",                        default: [],                 array: true
    t.string   "other_major"
    t.string   "mobile_number"
    t.string   "bio"
    t.boolean  "interested_in_online_teachings", default: false
    t.boolean  "interested_in_online_lessons",   default: false
    t.integer  "users_viewed",                   default: [],                 array: true
    t.boolean  "verified",                       default: false
    t.string   "verification_request_id"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "date_of_birth"
    t.string   "user_name"
    t.string   "jid"
    t.float    "rating"
    t.integer  "pin"
    t.datetime "pin_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "version_controls", force: :cascade do |t|
    t.string   "ios_version",      limit: 25
    t.string   "android_version",  limit: 25
    t.boolean  "ios_enforced",                default: false
    t.boolean  "android_enforced",            default: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
end
