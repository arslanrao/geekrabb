class AddDeviceTypeToUserIdentity < ActiveRecord::Migration[5.0]
  def change
    add_column :user_identities, :device_type, :string, limit: 20
  end
end
