class AddMobilePaymentColumnsInPayment < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :state, :string
    add_column :payments, :mobile_payment, :boolean, default: false
  end
end
