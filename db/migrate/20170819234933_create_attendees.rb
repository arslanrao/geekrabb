class CreateAttendees < ActiveRecord::Migration[5.0]
  def change
    create_table :attendees do |t|

      t.integer :activity_id, index: true
      t.integer :user_id, index: true
      t.boolean :going, default: false

      t.timestamps
    end
  end
end
