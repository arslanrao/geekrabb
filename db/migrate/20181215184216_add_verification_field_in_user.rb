class AddVerificationFieldInUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :pin, :integer
    add_column :users, :pin_sent_at, :datetime
  end
end
