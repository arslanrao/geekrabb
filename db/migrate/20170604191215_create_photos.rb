class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
      t.integer :attachable_id
      t.string :attachable_type
      t.timestamps null: false
    end
  end
end
