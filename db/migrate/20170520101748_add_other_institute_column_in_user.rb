class AddOtherInstituteColumnInUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :other_institute, :string
  end
end
