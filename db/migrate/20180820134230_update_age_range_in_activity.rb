class UpdateAgeRangeInActivity < ActiveRecord::Migration[5.0]
  def up
    add_column :activities, :age_range, :text, array: true
    remove_column :activities, :age_start
    remove_column :activities, :age_end
  end

  def down
    remove_column :activities, :age_range
    add_column :activities, :age_start, :integer
    add_column :activities, :age_end, :integer
  end
end
