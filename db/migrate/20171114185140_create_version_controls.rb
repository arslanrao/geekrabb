class CreateVersionControls < ActiveRecord::Migration[5.0]
  def change
    create_table :version_controls do |t|

      t.string :ios_version, limit: 25
      t.string :android_version, limit: 25
      t.boolean :ios_enforced, default: false
      t.boolean :android_enforced, default: false

      t.timestamps
    end
  end
end
