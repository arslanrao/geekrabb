class AddPaypalColumnsToPayment < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :paymentToken, :string
    add_column :payments, :payerID, :string
    add_column :payments, :paymentID, :string
    add_column :payments, :intent, :string
    add_column :payments, :returnUrl, :string
    add_column :payments, :payment_type, :string
  end
end
