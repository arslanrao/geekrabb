class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :account_type
      t.string :gender
      t.integer :age
      t.string :city
      t.string :education_level
      t.string :institution
      t.string :major
      t.datetime :availablity_start_time
      t.datetime :availablity_end_time
      t.float :hourly_rate
      t.boolean :status, default: true

      t.timestamps
    end
  end
end
