class AddAgeAndGenderToActivity < ActiveRecord::Migration[5.0]
  def change
    add_column :activities, :gender, :string, limit: 10
    add_column :activities, :age_start, :integer
    add_column :activities, :age_end, :integer
  end
end
