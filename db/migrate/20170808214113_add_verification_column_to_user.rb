class AddVerificationColumnToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :verified, :boolean, default: false
    add_column :users, :verification_request_id, :string
  end
end
