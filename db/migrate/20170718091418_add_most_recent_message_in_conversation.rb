class AddMostRecentMessageInConversation < ActiveRecord::Migration[5.0]
  def change
    add_column :conversations, :recent_message, :text
  end
end
