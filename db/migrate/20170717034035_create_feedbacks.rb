class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|

      t.float :rating
      t.text :comment
      t.integer :user_id
      t.integer :rated_id
      t.timestamps
    end
  end
end
