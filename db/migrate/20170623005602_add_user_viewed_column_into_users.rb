class AddUserViewedColumnIntoUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :users_viewed, :integer, array: true, default: []
  end
end
