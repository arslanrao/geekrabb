class AddEmployeeAndHobbyUsersColumnsInUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :nationality, :string
    add_column :users, :work_experience, :float
    add_column :users, :job, :string
    add_column :users, :teaching_interests, :string, array: true, default: []
    add_column :users, :languages, :string, array: true, default: []
    add_column :users, :teaching_modules, :string, array: true, default: []
    add_column :users, :hobbies, :string, array: true, default: []
  end
end
