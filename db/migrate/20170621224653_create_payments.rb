class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|

      t.string :transaction_id
      t.integer :user_id
      t.string :response_code
      t.string :response_message
      t.string :transaction_amount
      t.string :transaction_currency
      t.string :last_4_digits
      t.string :first_4_digits
      t.string :card_brand
      t.string :trans_date
      t.string :secure_sign
      t.timestamps
    end

    add_index :payments, :user_id
  end
end
