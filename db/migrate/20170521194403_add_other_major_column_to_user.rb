class AddOtherMajorColumnToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :other_major, :string
  end
end
