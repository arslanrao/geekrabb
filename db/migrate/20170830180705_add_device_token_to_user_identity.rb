class AddDeviceTokenToUserIdentity < ActiveRecord::Migration[5.0]
  def change
    add_column :user_identities, :device_token, :string
  end
end
