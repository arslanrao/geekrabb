class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|

      t.string :name
      t.text :detail
      t.float :latitude
      t.float :longitude
      t.integer :user_id, index: true
      t.boolean :status, default: true
      t.datetime :starting_time

      t.timestamps
    end
  end
end
