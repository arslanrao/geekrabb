class CreateDegrees < ActiveRecord::Migration[5.0]
  def change
    create_table :degrees do |t|
      t.string :name
      t.string :institution
      t.string :other_institute
      t.references :user, index: true
      t.timestamps
    end
  end
end
