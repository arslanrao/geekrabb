class AddAttachmentAvatarToDegree < ActiveRecord::Migration[5.0]
  def self.up
    change_table :degrees do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :degrees, :avatar
  end
end
