class AddedColumnsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :interested_in_online_teachings, :boolean, default: false
    add_column :users, :interested_in_online_lessons, :boolean, default: false
  end
end
