class CreateProfileViews < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_views do |t|

      t.integer :user_viewed_id
      t.integer :user_id
      t.integer :count, default: 0

      t.timestamps
    end
  end
end
