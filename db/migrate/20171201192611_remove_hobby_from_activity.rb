class RemoveHobbyFromActivity < ActiveRecord::Migration[5.0]
  def change
    remove_column :activities, :hobby, :string
  end
end
