# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@geekrabb.com', password: 'admin321', password_confirmation: 'admin321') if Rails.env.development?

version_control = VersionControl.find_or_initialize_by(ios_version: "0.0.0", android_version: "0.0.0", ios_enforced: false,android_enforced: false)
version_control.save!
