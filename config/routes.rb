Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  apipie

  get '/user_info_for_chat' => 'users#info_for_chat', format: :js

  scope '(:locale)' do
    get 'users/sign_up' => 'home#register'
  end

  devise_for :users, controllers: {sessions: "sessions"}

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      devise_for :users, only: [:sessions, :registrations ], controllers: {sessions: 'api/v1/devise/sessions', registrations: 'api/v1/devise/registrations'}
      resources :users do
        get :default_values, on: :collection
        get :search, on: :collection
        get :me, on: :collection
        get :display_contact_info, on: :collection
        post :verify_payment, on: :collection
        get :verify_account, on: :collection
        get :resend_verification_code, on: :collection
        get :subscribe_for_notification, on: :collection
        get :reset_password, on: :collection
        post :update_password, on: :collection
        post :update_profile, on: :collection
        put :update_user, on: :collection
        post :send_notification, on: :collection
      end

      resources :conversations, only: [:index, :create, :show, :destroy] do
        get :latest_messages
        post :read
        collection do
          get :unread_count
        end
      end
      resources :messages, only: [:create]
      resources :contacts, only: [:create]
      resources :feedbacks, only: [:create]
      resources :activities do
        member do
          get :join
          get :leave
        end
      end
    end
  end

  scope '(:locale)' do
    resources :users do
      post :update_payment_status
      get :get_contact_info, format: :js
      get :follow, format: :js
      get :unfollow, format: :js
    end

    resources :conversations do
      resources :messages
    end

    resources :feedbacks

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root "home#index"
    get "/register" => 'home#register'
    post "/register_user" => 'home#register_user', format: :js
    post "/verify_code" => 'home#verify_code', format: :js
    get "/verify" => 'users#verify_phone_number', as: 'new_verify'
    post '/verify' => 'users#resend_code', as: 'resend_verify'
    put '/verify' => 'users#verify_code', as: 'verify'
    get "/summary" => 'home#summary'
    get "/search" => 'home#search'
    post "/search" => 'home#search'
    get "/mydetail" => 'home#mydetail'
    get "/mysubject" => 'home#mysubject'
    get "/myenquiry" => 'home#myenquiry'
    get "/mypurchase" => 'home#mypurchase'
    get "/online-lesson" => 'home#online_lesson'
    get "/feedback_section" => 'home#feedback_section'
    get "/found-me" => 'home#found_me'
    get "/suggested-teacher" => 'home#suggested_teacher'
    get "/profile-photo" => 'home#profile_photo'
    get "/post-question" => 'home#post_question'
    get "/followers" => 'home#followers'
    get "/tags" => 'home#tags'
    get "/history" => 'home#history'
    get "/privacy" => 'home#privacy'
    get "/contactus" => 'home#contactus'
    get "/aboutus" => 'home#aboutus'
    get "/payment" => 'home#payment'
  end
end
