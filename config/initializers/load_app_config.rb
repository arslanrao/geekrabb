raw_config = File.read("#{Rails.root}/config/app_config.yml")
APP_CONFIG = ActiveSupport::HashWithIndifferentAccess.new YAML.load(raw_config)[Rails.env]
