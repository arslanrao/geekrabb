class SessionsController < Devise::SessionsController
  skip_before_filter :redirect_if_unverified
end
