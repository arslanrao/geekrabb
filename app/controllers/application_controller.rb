require 'houston'
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  protect_from_forgery except: :get_contact_info
  before_action :redirect_if_unverified
  before_action :set_locale
  before_action :set_header_variables

  def set_header_variables
    @recent_students = User.students.last(5)

    @popular_majors = frequency(User.pluck(:major)-[nil]).sort_by(&:last).reverse.last(5)
  end

  def send_notification(data, key, notification_hash, user_id, device_id)
    fcm = FCM.new(APP_CONFIG[:push_notification][:firebase_key])
    device_token = UserIdentity.where(user_id: user_id, device_id: device_id).pluck(:device_token).compact
    options = { data: data, collapse_key: key, notification: notification_hash }
    response = fcm.send(device_token, options)
  end

  def send_ios_notification(data, key, token)
    apn = Houston::Client.development
    file_path = [Rails.root, 'lib/assets', APP_CONFIG[:push_notification][:ios_certificate_file_path]].join('/')
    apn.certificate = File.read(file_path)
    #token = UserIdentity.where(user_id: user_id, device_id: device_id).pluck(:device_token).compact
    #token = "<505b49e4 91b085f7 6c40d99d ac98290d 87360fa6 1ca594d6 97a39ec3 44af8e50>"
    notification = Houston::Notification.new(device: token)
    notification.alert = data[:message]
    notification.badge = 0
    notification.content_available = true
    notification.mutable_content = true
    notification.custom_data = data
    apn.push(notification)
  end

  def set_locale
    begin
      I18n.locale = params[:locale] || I18n.default_locale
    rescue
      I18n.locale = I18n.default_locale
    end
  end

  def default_url_options(options = {})
    { locale: I18n.locale }
  end

  private
  def frequency(a)
    a.group_by do |e|
      e
    end.map do |key, values|
      [key, values.size]
    end
  end

  def redirect_if_unverified
    if user_signed_in? && !current_user.verified
      redirect_to new_verify_url, notice: "Please verify your phone number"
    end
  end
end
