class Api::V1::ConversationsController < ApiController
  include Concerns::UserAuthentication
  before_action :set_conversation, only: [:show, :destroy]
  before_action :set_page, only: [:show]

  def index
    conversations = @current_user.recent_related_conversations
    success(data: ActiveModelSerializers::SerializableResource.new(conversations, each_serializer: ConversationSerializer, scope: {current_user: @current_user}))
  end

  def create
    conversation = Conversation.between(@current_user.id, params[:conversation][:recipient_id]).first
    if conversation.blank?
      recipient_id = params[:conversation][:recipient_id]
      conversation = Conversation.create(sender_id: @current_user.id, recipient_id: recipient_id)
    end

    if conversation.persisted?
      created(data: ConversationSerializer.new(conversation, scope: {current_user: @current_user}))
    else
      unprocessable_entity(conversation.errors)
    end
  end

  def show
    messages = @conversation.messages.includes(:user).page(@page)
    success(data: ActiveModelSerializers::SerializableResource.new(@conversation, serializer: ConversationDetailSerializer, scope: {current_user: @current_user, messages: messages}))
  end

  def destroy
    @conversation.destroy ? success : internal_server_error
  end

  def latest_messages
    @conversation = Conversation.find(params[:conversation_id])
    messages = @conversation.messages.includes(:user).latest_to(params[:message_id].to_i)
    success(data: ActiveModelSerializers::SerializableResource.new(messages, each_serializer: MessageSerializer))
  end

  api :POST, '/conversations/:conversation_id/read', "Set read to all the messages before message_id's message"
  formats ['json']
  description "Set read to all the messages before message_id's message"
  param :message_id, String, desc: "Id of message before which all messages are to set as read", required: true
  def read
    @conversation = Conversation.find(params[:conversation_id])
    if @conversation.mark_messages_read(params[:message_id], @current_user.id)
      return render json: {success: true}, status: :ok
    else
      return render json: {success: false}, status: :not_accepted
    end
  end

  api :GET, '/conversations/unread_count', "Return the conversations count with unread messages"
  formats ['json']
  description "Return the conversations count with unread messages"
  def unread_count
    conversation_count = @current_user.convo_with_unread_mesg.count

    render json: { success: true, conversation_count: conversation_count }, status: :ok
  end

  private

    def set_conversation
      @conversation = Conversation.find(params[:id])
    end

    def set_page
      @page = params[:page].present? ? params[:page] : @conversation.messages.page(1).total_pages
    end
end
