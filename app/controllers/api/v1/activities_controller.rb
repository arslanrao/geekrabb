class Api::V1::ActivitiesController < ApiController
  include Concerns::UserAuthentication
  include HomeHelper

  before_action :set_activity, only: [:show, :update, :join, :leave]
  before_action :validate_joined, only: [:join]
  before_action :validate_leave, only: [:leave]
  load_and_authorize_resource

  resource_description do
    short "A activity is an event or gathering which is created by a user and any user can join it"
  end

  def_param_group :activity_param_group do
    param :activity, Hash, required: true do
      param :name, String, required: true
      param :location, String, required: true
      param :detail, String, required: true
      param :latitude, String
      param :longitude, String
      param :starting_time, String, required: true
      param :status, [true, false, 'true', 'false']
      param :gender, ['Male', 'Female', 'Both']
      param :age_range, String
      param :attendees_attributes, Array, desc: 'Attendees Attributes' do
        param :id, Hash, desc: 'Attendees Attributes' do
          param :id, Integer
          param :user_id, Integer
          param :going, [true, false, 'true', 'false']
          param :_destroy, [true, false, 'true', 'false']
        end
      end
    end
  end

  api :GET, '/activities', "Return  all activities near user location"
  formats ['json']
  description 'Return all activities near user location'
  param :latitude, String
  param :longitude, String
  def index
    activities = if params[:latitude] && params[:longitude]
      distance = params[:distance] || 25

      activities = Activity.all.near([params[:latitude].to_f, params[:longitude].to_f], distance, units: :km)
      Activity.where(id: activities.map(&:id))
    else
      Activity.ransack(params[:q]).result
    end

    activities = activities.page(params[:page]).per(Activity::PER_PAGE).includes(attendees: { user: :photo })
    render json: activities,  each_serializer: ActivitySerializer
  end

  api :GET, '/activities/:id', "Return activity object"
  formats ['json']
  description 'Return activity object'
  def show
    render json: {activity: ActivitySerializer.new(@activity, { eager_load_attendees: true })}, status: :ok
  end

  api :POST, '/activities', "Create activity object"
  param_group :activity_param_group
  formats ['json']
  description 'Create activity object'
  def create
    activity = Activity.new activity_params
    activity.user_id = current_user.id
    activity.device_id = @user_identity.device_id

    if activity.save
      return render json: {success: true, activity: ActivitySerializer.new(activity)}, status: :created
    else
      render json: {success: false, errors: activity.errors}, status: :forbidden
    end
  end

  api :PUT, '/activities/:id', "Update activity and its attendees"
  param_group :activity_param_group
  formats ['json']
  description 'Update activity and its attendees'
  def update
    if @activity.update(activity_params)
      render json: { activity: ActivitySerializer.new(@activity), message: "Activity updated successfully" }, status: :ok
    else
      render json: { activity: ActivitySerializer.new(@activity), message: @activity.errors.full_messages.to_sentence }, status: :forbidden
    end
  end

  api :GET, '/activities/:id/join', "Add current user as attendee in the activity"
  formats ['json']
  description 'Add current user as attendee in the activity'
  def join
    attendee = @activity.attendees.find_or_initialize_by(user_id: @current_user.id)
    attendee.going = params[:going]

    if attendee.save
      render json: attendee.user, serializer: AttendeeSerializer, going: attendee.going, message: "You joined activity successfully" , status: :ok
    else
      render json: { message: attendee.errors.full_messages.to_sentence }, status: :forbidden
    end
  end

  api :GET, '/activities/:id/leave', "Remove current user as attendee from the activity"
  formats ['json']
  description 'Remove current user as attendee from the activity'
  def leave
    attendee = @activity.attendees.find_by(user_id: @current_user.id)
    if attendee.destroy
      render json: { activity: ActivitySerializer.new(@activity), message: "You left activity successfully" }, status: :ok
    else
      render json: { activity: ActivitySerializer.new(@activity), message: @activity.errors.full_messages.to_sentence }, status: :forbidden
    end
  end

  api :DELETE, '/activities/:id', "Destroy activity and all its attendees"
  formats ['json']
  description 'Destroy activity and all its attendees'
  def destroy
    @activity.destroy ? success : internal_server_error
  end

  private
  def activity_params
    params.require(:activity).permit(:name, :location, :detail, :latitude, :longitude, :starting_time, :status, :gender, :age_range, attendees_attributes: [:id, :user_id, :going, :_destroy])
  end

  def validate_joined
    attendee = @activity.attendees.where(user_id: @current_user.id, going: true)
    if attendee.present?
      return render json: { message: "You have already joined this activity" }, status: :forbidden
    end
  end

  def validate_leave
    attendee = @activity.attendees.where(user_id: @current_user.id, going: true)
    if attendee.blank?
      return render json: { activity: ActivitySerializer.new(@activity), message: "You have not joined this activity before. Please join activity first" }, status: :ok
    end
  end

  def set_activity
    @activity = Activity.find_by(id: params[:id])
    not_found('Activity not found') if @activity.blank?
  end
end
