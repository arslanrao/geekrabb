require 'nexmo'
class Api::V1::Devise::RegistrationsController < Devise::RegistrationsController
  include Concerns::UserAuthentication

  skip_before_action :authenticate_user, only: [:create]
  skip_before_action :set_user, only: [:create]
  before_action :validate_signup_params, only: [:create]
  before_action :set_device_type, only: [:create]

  resource_description do
    short "Handle user's registrations process"
  end

  def_param_group :sign_up_params_group do
    param :user, Hash, required: true do
      param :email, String, required: true
      param :device_id, String, required: true
      param :first_name, String, required: true
      param :last_name, String, required: true
      param :password, String, required: true
      param :account_type, String, required: true, desc: "Valid account types values are in #{User::ACCOUNT_TYPES}"
      param :city, String, required: true
      param :gender, String
      param :education_level, String
      param :institution, String
      param :major, String
      param :other_major, String
      param :nationality, String
      param :work_experience, String
      param :hourly_rate, String
      param :date_of_birth, String
      param :job, String
      param :mobile_number, String
      param :other_institute, String
      param :availablity_start_time, String
      param :availablity_end_time, String
      param :hobbies, Array, of: String
      param :languages, Array, of: String
      param :latitude, Float
      param :longitude, Float
      param :teaching_interests, Array, of: String
      param :teaching_modules, Array, of: String
      param :interested_in_online_lessons, [true, false, "true", "false"]
      param :interested_in_online_teachings, [true, false, "true", "false"]
      param :degrees_attributes, Hash do
        param :id, Hash do
          param :name, String, required: true
          param :institution, String, required: true
          param :other_institute, String
          param :_destroy, [true, false, "true", "false"], required: true
        end
      end
    end
  end

  api :POST, '/users', "User Sign-Up"
  param_group :sign_up_params_group
  formats ['json']
  description 'User can sign up'
  def create
    resource = User.new sign_up_params
    if resource.save
      resource.create_openfire_user(params[:user][:password])
      unless resource.tutor?
        resource.send_pin!
      end

      access_token = resource.generate_access_token(params[:user][:device_id], @device_type)
      if access_token.present?
        sign_in(:user, resource)
      end

      return render json: {success: true, user_id: resource.id, message: "Your account is created successfully but still needs to be verified. Please enter verification code sent at your mobile number #{resource.mobile_number}"}, status: :created
    else
      warden.custom_failure!
      resource.date_of_birth_validness(sign_up_params[:date_of_birth])
      render json: {success: false, errors: resource.errors}, status: :unprocessable_entity
    end
  end

  private

  def sign_up_params
    allow = [:email, :first_name, :last_name, :user_name, :password, :account_type, :city, :gender, :education_level, :institution, :major, :nationality, :work_experience, :hourly_rate, :date_of_birth, :job, :other_institute, :other_major, :languages, :availablity_start_time, :availablity_end_time, :interested_in_online_lessons, :interested_in_online_teachings, :mobile_number, :latitude, :longitude, [teaching_modules: []], [teaching_interests: []], [degrees_attributes: [:id, :name, :institution, :other_institute, :_destroy]], [hobbies: []]]

    params.require(:user).permit(allow)
  end

  def invalid_signup_attempt error=nil
    error = 'Invalid signup attempt' if error.blank?
    render json: {success: false, error: error}, status: :unauthorized
  end

  def validate_signup_params
    begin
      invalid_signup_attempt if sign_up_params.blank? || params[:user][:device_id].blank?
    rescue ActionController::ParameterMissing
      invalid_signup_attempt
    end
  end

end
