class Api::V1::ContactsController < ApiController
  include Concerns::UserAuthentication

  resource_description do
    short "Contact us with the site owner"
  end

  def_param_group :contacts_group do
    param :contact, Hash, required: true do
      param :subject, String, required: true
      param :message, String, required: true
    end
  end

  api :POST, '/contacts', "User can create a contact request"
  param_group :contacts_group
  formats ['json']
  description 'User can create a contact request'
  def create
    contact = Contact.new(contact_params)
    contact.email = @current_user.email

    if contact.save
      created(data: ContactSerializer.new(contact))
    else
      unprocessable_entity(contact.errors)
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:subject, :message)
  end
end
