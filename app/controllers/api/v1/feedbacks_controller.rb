class Api::V1::FeedbacksController < ApiController
  include Concerns::UserAuthentication

  resource_description do
    short "Feedback for a specific user"
  end

  def_param_group :feedbacks_group do
    param :feedback, Hash, required: true do
      param :comment, String, required: true
      param :rated_id, String, required: true
      param :rating, String, required: true
    end
  end

  api :POST, '/feedbacks', "User can create a feedback"
  param_group :feedbacks_group
  formats ['json']
  description 'User can create a feedback'
  def create
    feedback = current_user.feedbacks.new(feedback_params)

    if feedback.save
      created(data: FeedbackSerializer.new(feedback))
    else
      unprocessable_entity(feedback.errors)
    end
  end

  private

  def feedback_params
    params.require(:feedback).permit(:comment, :rated_id, :rating)
  end
end
