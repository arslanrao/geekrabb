class Api::V1::UsersController < ApiController
  include Concerns::UserAuthentication
  include HomeHelper

  skip_before_action :authenticate_user, only: [:default_values, :verify_account, :resend_verification_code, :reset_password]
  skip_before_action :set_user, only: [:default_values, :verify_account, :resend_verification_code, :reset_password]
  before_action :set_user_object, only: [:show]
  before_action :set_payment, only: [:verify_payment]
  before_action :set_user_by_email, only: [:reset_password]
  before_action :validate_current_password, only: [:update_password]
  before_action :set_device_type, only: [:verify_account]
  load_and_authorize_resource

  resource_description do
    short "A user register itself, interacts with tutors, students, hobbyist and employees"
  end

  def_param_group :update_user_group do
    param :user, Hash, required: true do
      param :email, String
      param :first_name, String
      param :last_name, String
      param :password, String
      param :account_type, String, desc: "Valid account types values are in #{User::ACCOUNT_TYPES}"
      param :city, String
      param :gender, String
      param :education_level, String
      param :institution, String
      param :major, String
      param :other_major, String
      param :nationality, String
      param :work_experience, String
      param :hourly_rate, String
      param :date_of_birth, String
      param :job, String
      param :mobile_number, String
      param :bio, String
      param :other_institute, String
      param :availablity_start_time, String
      param :availablity_end_time, String
      param :hobbies, Array, of: String
      param :languages, Array, of: String
      param :latitude, Float
      param :longitude, Float
      param :teaching_interests, Array, of: String
      param :teaching_modules, Array, of: String
      param :interested_in_online_lessons, [true, false, 'true', 'false']
      param :interested_in_online_teachings, [true, false, 'true', 'false']
      param :degrees_attributes, Hash, desc: 'Degrees Attributes' do
        param :id, String
        param :name, String
        param :institution, String
        param :other_institute, String
        param :_destroy, String
      end
      param :photo_attributes, Hash, desc: 'Photo Attributes' do
        param :id, String
        param :avatar, String
        param :_destroy, String
      end
    end
  end

  api :PUT, '/update_user', "User can update its profile"
  param_group :update_user_group
  formats ['json']
  description 'User can update its profile'
  def update_user
    if @current_user.update(user_params)
      @current_user.update_openfire_user(params[:user][:password])
      render json: { user: UserSerializer.new(@current_user), message: "Profile updated successfully" }, status: :ok
    else
      render json: { user: UserSerializer.new(@current_user), message: @current_user.errors.full_messages.to_sentence }, status: :unauthorized
    end
  end

  api :POST, '/users/update_password', "Update password of given user"
  formats ['json']
  description 'Update password of given user'
  param :current_password, String, desc: "Current password of user whose password has to be changed", required: true
  param :new_password, String, desc: "New password of user which is to be set", required: true
  def update_password
    @current_user.password = params[:new_password] if params[:new_password].present?
    if @current_user.save
      @current_user.update_openfire_user(params[:user][:new_password])
      return render json: {success: true}, status: :ok
    else
      return render json: {success: false, error: @current_user.errors.full_messages.to_sentence}, status: :not_acceptable
    end
  end

  api :GET, '/users/reset_password', "Reset password of given user"
  formats ['json']
  description 'Reset password of given user'
  param :email, String, desc: "Email of user whose password has to be reset", required: true
  def reset_password
    if @user.send_reset_password_instructions
      return render json: {success: true}, status: :ok
    else
      return render json: {success: false}, status: :not_acceptable
    end
  end

  api :POST, '/users/update_profile', "Update profile of current user"
  formats ['json']
  description 'Update profile of current user'
  param :image, Object, desc: "User profile image", required: true
  def update_profile
    previous_photo = @current_user.photo
    @current_user.build_photo
    @current_user.photo.avatar = params[:image]

    if @current_user.save
      @current_user.update_openfire_user()
      previous_photo.try(:destroy)
      return render json: {success: true, image_url: @current_user.avatar_url}, status: :ok
    else
      return render json: {success: false, error: @current_user.errors.full_messages.to_sentence}, status: :not_acceptable
    end
  end

  api :GET, '/users/default_values', "Return available option values for registeration"
  formats ['json']
  description 'Return available option values for registeration'
  def default_values
    render json: {account_types: options_for_account_type, genders: options_for_gender, teaching_interests: options_for_teaching_interests, languages: options_for_languages, teaching_modules: options_for_teaching_modules, hobbies: options_for_hobbies, jobs: options_for_job, nationalities: options_for_nationality, cities: options_for_city, education_levels: options_for_education_level, institutions: options_for_institution, majors: options_for_major, time_slots: options_for_time}, status: :ok
  end

  api :GET, '/users/search', "Return searched users results"
  formats ['json']
  description 'Return searched users results'
  def search
    @q = User.ransack(params[:q])
    @users = @q.result.where.not(id: @current_user.id)
    @users = @users.page(params[:page]).per(User::PER_PAGE)
    serialized_users = @users.map { |user| UserListSerializer.new(user) }
    render json: {users: serialized_users}, status: :ok
  end

  api :GET, '/users/:id', "Return user object"
  formats ['json']
  description 'Return user object'
  def show
    @user.ratings.includes(:user)
    render json: {user: UserSerializer.new(@user)}, status: :ok
  end

  api :GET, '/users/me', "Return current user object"
  formats ['json']
  description 'Return current user object'
  def me
    render json: {user: UserSerializer.new(@current_user)}, status: :ok
  end

  api :GET, '/users/display_contact_info', "Return result to display contact info to user or not"
  formats ['json']
  description 'Return result to display contact info to user or not'
  param :user_id, String, desc: "Id of user whose contact info is to be displayed ", required: true
  def display_contact_info
    if @current_user.users_viewed.length < User::MAX_USERS_CONTACT_INFO_VIEWS || @current_user.payments.successfull.present?
      unless @current_user.users_viewed.include?(params[:user_id].to_i)
        @current_user.users_viewed.push(params[:user_id])
        @current_user.save(validate: false)
      end
      return render json: {success: true}, status: :ok
    else
      return render json: {success: false}, status: :ok
    end
  end

  api :POST, '/users/verify_payment', "Verify mobile payment from paypal"
  formats ['json']
  description 'Verify mobile payment from paypal'
  param :user_id, String, desc: "Id of user whose payment has to be verified", required: true
  def verify_payment
    @user = User.find_by(id: params[:user_id])

    if @user.present? && @user.payments.create(payment_type: "Paypal", paymentID: @payment_id, state: @payment.state, mobile_payment: true)

      if @payment.state = 'approved'
        render json: { message: "Your payment was successfull" }, status: :ok
      else
        render json: { message: "Sorry! Your payment was not successfull yet" }, status: :ok
      end
    end
  end

  api :GET, '/users/verify_account', "Verify account by code sent on user's mobile number"
  formats ['json']
  description "Verify account by code sent on user's mobile number"
  param :user_id, String, desc: "Id of user whose account has to be verified", required: true
  param :code, String, desc: "4 digits code that user received on mobile number for account verification", required: true
  def verify_account
    @user = User.find_by(id: params[:user_id])

    if @user.present? && !@user.verified? || params[:code].blank?
      if Time.now > @user.pin_sent_at.advance(minutes: 60)
        render json: { message: "Your pin has expired. Please request another." }, status: :not_acceptable
      elsif params[:code].try(:to_i) == @user.pin
        @user.update(verified: true)

        access_token = @user.generate_access_token([:device_id], @device_type)
        sign_in(:user, @user)
        return render json: {success: true, access_token: access_token, user: UserSerializer.new(@user)}, status: :accepted
      else
        render json: { message: "Sorry! This code is invalid. Please try again" }, status: :not_acceptable
      end
    else
      render json: { message: "Sorry! This code is invalid. Please try again" }, status: :not_acceptable
    end
  end

  api :GET, '/users/resend_verification_code', "Resend verification code on user's mobile number"
  formats ['json']
  description "Resend verification code on user's mobile number"
  param :user_id, String, desc: "Id of user whose account has to be verified", required: true
  def resend_verification_code
    @user = User.find_by(id: params[:user_id])

    if @user.present? && !@user.verified?
      @user.send_pin!

      return render json: {success: true, message: "We have successfully resent a verification code on your mobile number #{@user.mobile_number}. Please enter it to activate your account"}, status: :ok
    else
      render json: { message: "Sorry! This user does not exist or already been verified" }, status: :not_acceptable
    end
  end

  api :GET, '/users/subscribe_for_notification', "Subscribe user for push notification"
  formats ['json']
  description "Subscribe user for push notification"
  param :device_id, String, desc: "Device id on which we need to subscribe push notification", required: true
  param :device_token, String, desc: "Token of the device that we need to subscribe", required: true
  def subscribe_for_notification
    identity = @current_user.user_identities.where(device_id: params[:device_id])
    if identity.present? && identity.update(device_token: params[:device_token])
      render json: {success: true, message: "We have successfully subscribed for push notification"}, status: :ok
    else
      render json: {success: false, message: "Sorry! Push notification subscription fails" }, status: :not_acceptable
    end
  end

  def send_notification
  end

  private
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :account_type, :gender, :date_of_birth, :city, :education_level, :institution, :major, :availablity_start_time, :availablity_end_time, :hourly_rate, :other_institute, :nationality, :work_experience, :job, :other_major, :mobile_number, :bio, :photo, :interested_in_online_lessons, :interested_in_online_teachings, :latitude, :longitude, teaching_interests: [], languages: [], teaching_modules: [], hobbies: [], degrees_attributes: [:id, :name, :institution, :other_institute, :_destroy], photo_attributes: [:id, :avatar, :_destroy])
  end

  def validate_current_password
    return render json: {success: false, error: 'Sorry! Please provide valid current password.'}, status: :not_acceptable unless @current_user.valid_password?(params[:current_password])
  end

  def set_user_object
    @user = User.find_by(id: params[:id])
    not_found('User not found') if @user.blank?
  end

  def set_user_by_email
    @user = User.find_by(email: params[:email])
    not_found('User not found') if @user.blank?
  end

  def set_payment
    @payment_id = params[:confirmation][:response][:id]

    PayPal::SDK.configure(
      mode: APP_CONFIG[:payment][:paypal_mode],
      client_id: APP_CONFIG[:payment][:paypal_client_id],
      client_secret: APP_CONFIG[:payment][:paypal_client_secret],
      ssl_options: { }
    )

    begin
      @payment = PayPal::SDK::REST::Payment.find(@payment_id)
    rescue
      return render json: { message: "Sorry! Your payment was not successfull yet" }, status: :ok
    end
  end

end
