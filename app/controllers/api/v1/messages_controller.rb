class Api::V1::MessagesController < ApiController
  include Concerns::UserAuthentication
  before_action :set_conversation, only: [:create]

  def create
    message = @conversation.messages.build(message_params)
    message.user = @current_user
    message.device_id = @user_identity.device_id

    if message.save
      created(data: MessageSerializer.new(message))
    else
      unprocessable_entity(message.errors)
    end
  end

  private

    def message_params
      params.require(:message).permit(:body, :conversation_id)
    end

    def set_conversation
      @conversation = Conversation.find(params[:message][:conversation_id])
    end
end
