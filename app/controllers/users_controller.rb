class UsersController < ApplicationController
  skip_before_filter :redirect_if_unverified, only: [:verify_phone_number, :resend_code, :verify_code]
  before_filter :authenticate_user!, except: [:update_payment_status]
  before_filter :set_user, only: [:show, :update, :get_contact_info, :follow, :unfollow]
  before_filter :update_view_count, only: [:show]
  before_filter :user_by_jid, only: [:info_for_chat]

  def show
    @feedbacks = Feedback.where(rated_id: @user.id).includes(:user)
    @feedback = current_user.feedbacks.build
  end

  def update
    if @user.update(user_params)
      @user.update_openfire_user(params[:user][:password])
      flash[:notice] = "Profile updated successfully"
      redirect_to mydetail_path
    else
      flash[:alert] = @user.errors.full_messages.to_sentence
      redirect_to mydetail_path
    end
  end

  def update_payment_status
    @user = User.find_by(id: params[:user_id])

    if @user.present? && @user.payments.create(payment_params)
      if (params[:payment_type] == 'Paypal') || (params[:payment_type] == 'Paytab' && params[:response_code] == '100')
        @success = true
        @message = "Your payment was successfull"
      else
        @success = false
        @message = params[:response_message]
      end
    end

    @back_url = params[:return_user_id].present? ? user_path(params[:return_user_id]) : root_path

    @current_page = 'payment'
    @current_user = @user
    render template: 'home/payment'
  end

  def get_contact_info
    if current_user.users_viewed.length < User::MAX_USERS_CONTACT_INFO_VIEWS || current_user.payments.successfull.present?
      unless current_user.users_viewed.include?(params[:user_id].to_i)
        current_user.users_viewed.push(params[:user_id])
        current_user.save(validate: false)
      end
      @payment = false
    else
      @payment = true
    end
  end

  def follow
    @type = 'followed'
    @result = current_user.follow!(@user)
  end

  def unfollow
    @type = 'unfollowed'
    @result = current_user.unfollow!(@user)
  end

  def info_for_chat
    image_url = @user.image_for_chat
    first_name = @user.first_name

    render json: { first_name: first_name, image_url: image_url }, status: :ok
  end

  def verify_phone_number
  end

  def resend_code
    current_user.send_pin!
    redirect_to new_verify_url, notice: "A PIN number has been sent to your phone"
  end

  def verify_code
    if Time.now > current_user.pin_sent_at.advance(minutes: 60)
      flash.now[:alert] = "Your pin has expired. Please request another."
      render :verify_phone_number and return
    elsif params[:code].try(:to_i) == current_user.pin
      current_user.update_attribute(:verified, true)
      redirect_to root_url, notice: "Your phone number has been verified!"
    else
      flash.now[:alert] = "The code you entered is invalid."
      render :verify_phone_number
    end
  end

  private

  def update_view_count
    return if current_user.id == @user.id
    profile_view = current_user.profile_views.where(user_viewed_id: @user.id).first_or_create
    profile_view.update(count: profile_view.count + 1)
  end

  def set_user
    @user = User.find_by(id: params[:id])
    redirect_to root_path if @user.blank?
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :account_type, :gender, :date_of_birth, :city, :education_level, :institution, :major, :availablity_start_time, :availablity_end_time, :hourly_rate, :other_institute, :nationality, :work_experience, :job, :other_major, :mobile_number, :bio, :photo, :interested_in_online_lessons, :interested_in_online_teachings, teaching_interests: [], languages: [], teaching_modules: [], hobbies: [], degrees_attributes: [:id, :name, :institution, :other_institute, :_destroy], photo_attributes: [:id, :avatar, :_destroy])
  end

  def payment_params
    params.permit(:transaction_id, :user_id, :response_code, :response_message, :transaction_amount, :transaction_currency, :last_4_digits, :first_4_digits, :card_brand, :trans_date, :secure_sign, :paymentToken, :payerID, :paymentID, :intent, :returnUrl, :payment_type)
  end

  def user_by_jid
    @user = User.find_by(jid: params[:jid])
  end
end
