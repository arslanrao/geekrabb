require 'nexmo'
class HomeController < ApplicationController

  skip_before_filter :redirect_if_unverified, only: [:register, :register_user]
  before_action :authenticate_user!, except: [:index, :register, :register_user, :contactus, :aboutus, :verify_code]
  before_action :set_page

  def index
    @q = User.ransack(params[:q])
  end

  def register
    @user = User.new
  end

  def register_user
    @user = User.new(user_params)
    if @user.save
      @user.create_openfire_user(user_params[:password])
      if @user.tutor?
        flash[:notice] = "Successfully Registered User"
      else
        @user.send_pin!
      end
    else
      @user.date_of_birth_validness(user_params[:date_of_birth])
      @errors = @user.errors.full_messages.to_sentence
    end
  end

  def verify_code
    @user = User.find_by(id: params[:user_id])

    if Time.now > @user.pin_sent_at.advance(minutes: 60)
      @verified = false
    elsif params[:code].try(:to_i) == @user.pin
      @verified = true
      @user.update(verified: true)
    else
      @verified = false
    end
  end

  def search
    params[:q] = {} if params[:q].blank?
    params[:q][:id_not_eq] = current_user.id
    @q = User.ransack(params[:q])

    @users = @q.result.includes(:photo).page(params[:page]).per(User::PER_PAGE)
  end

  def summary
  end

  def mydetail
    current_user.build_photo if current_user.photo.blank?
  end

  def mysubject
  end

  def myenquiry
  end

  def mypurchase
    @payments = current_user.payments.successfull
  end

  def online_lesson
  end

  def feedback_section
    @feedbacks = Feedback.where(rated_id: current_user.id).includes(:user)
  end

  def found_me
    @profile_views = ProfileView.includes(:user).where(user_viewed_id: current_user.id)
    @this_months_views = @profile_views.this_month
    @recent_views = @profile_views.recents
  end

  def suggested_teacher
    params[:q] = {}
    params[:q][:id_not_eq] = current_user.id
    params[:q][:account_type_eq] = current_user.account_type
    params[:q][:city_eq] = current_user.city
    params[:q][:major_eq] = current_user.major
    params[:q][:teaching_modules_in] = current_user.teaching_modules
    params[:q][:job_eq] = current_user.job
    params[:q][:hobbies_cont_any] = current_user.hobbies

    @q = User.ransack(params[:q])
    @users = @q.result.page(params[:page]).per(7)
  end

  def profile_photo
  end

  def post_question
  end

  def followers
    @followees = current_user.followees(User)
  end

  def tags
  end

  def history
  end

  def privacy
  end

  def contactus
  end

  def aboutus
  end

  def payment
    @current_user = current_user
  end

  private

  def set_page
    @current_page = params[:action]
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :user_name, :password, :account_type, :gender, :date_of_birth, :city, :education_level, :institution, :major, :availablity_start_time, :availablity_end_time, :hourly_rate, :other_institute, :nationality, :work_experience, :job, :other_major, :interested_in_online_teachings, :interested_in_online_lessons, :mobile_number, teaching_interests: [], languages: [], teaching_modules: [], hobbies: [], degrees_attributes: [:id, :name, :institution, :other_institute, :avatar, :_destroy])
  end

end
