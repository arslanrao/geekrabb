class FeedbacksController < ApplicationController
  before_filter :authenticate_user!

  def create
    current_user.feedbacks.create(feedback_params)
    redirect_to user_path(params[:rated_id])
  end

  private
  def feedback_params
    params.permit(:comment, :rated_id, :rating)
  end
end
