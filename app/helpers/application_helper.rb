module ApplicationHelper

  def render_date datetime
    return if datetime.blank?
    datetime.strftime('%d/%m/%Y')
  end
end
