module UsersHelper
  def fetch_user_mobile_number user
    return user.mobile_number if user.mobile_number.present?
    "N/A"
  end

  def fetch_photo user, size
    return user.photo.avatar.url(size.to_sym) if user.photo.present?
    return 'prifile-image-male.png' if user.male?
    return 'prifile-image-female.png' if user.female?
    'prifile-image.jpg'
  end

  def boolean_to_human bool
    return 'Yes' if bool
    'No'
  end

  def image_for_chat user
    image = if user.blank?
      'prifile-image.jpg'
    elsif user.photo.present?
      user.photo.avatar.url(:profile)
    elsif user.male?
      'prifile-image-male.png'
    elsif user.female?
      'prifile-image-female.png'
    else
      'prifile-image.jpg'
    end

    asset_url(image)
  end
end
