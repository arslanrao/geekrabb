class ConversationDetailSerializer < ActiveModel::Serializer
  attributes :id, :sender_id, :recipient_id, :conversation_name, :messages, :sender_photo, :recipient_photo

  def conversation_name
    scope[:current_user] == object.sender ? object.recipient.username : object.sender.username
  end

  def messages
    ActiveModelSerializers::SerializableResource.new(scope[:messages], each_serializer: MessageSerializer, root: false)
  end

  def recipient_photo
    return if object.recipient.blank?

    photo_obj = {}
    missing_pic = object.recipient.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.recipient.photo.present?
      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.recipient.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end

  def sender_photo
    photo_obj = {}
    missing_pic = object.sender.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.sender.photo.present?
      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.sender.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end
end
