class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :user_name, :jid, :account_type, :gender, :date_of_birth, :city, :education_level, :institution, :major, :availablity_start_time, :availablity_end_time, :hourly_rate, :status, :email, :other_institute, :nationality, :work_experience, :job, :teaching_interests, :languages, :teaching_modules, :hobbies, :other_major, :mobile_number, :bio, :interested_in_online_teachings, :interested_in_online_lessons, :users_viewed, :photo, :degrees, :created_at, :last_sign_in_at, :latitude, :longitude, :rating, :verified

  has_many :ratings

  def photo
    photo_obj = {}
    missing_pic = object.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.photo.present?

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end
end
