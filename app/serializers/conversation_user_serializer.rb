class ConversationUserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :photo

  def photo
    photo_obj = {}
    missing_pic = object.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.photo.present?
      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end
end
