class ActivitySerializer < ActiveModel::Serializer
  attributes :id, :name, :detail, :latitude, :longitude, :location, :starting_time, :user_id, :status, :gender, :age_range, :attendees

  def attendees
    attendees = if instance_options[:eager_load_attendees]
      object.attendees.includes(user: :photo)
    else
      object.attendees
    end

    attendees.map {|u| UserListSerializer.new(u.user, { going: u.going })}
  end

end
