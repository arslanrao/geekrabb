class AttendeeSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :account_type, :photo, :going

  def photo
    photo_obj = {}

    medium_url = object.photo.avatar.url('medium') if object.photo && object.photo.avatar
    photo_obj[:medium] = if medium_url.present?
      [ActionController::Base.asset_host, medium_url].join()
    else
      missing_pic = object.male? ? 'profile-image-male.png' : 'profile-image-female.png'
      [ActionController::Base.asset_host, "/images/missing/medium/", missing_pic].join()
    end

    photo_obj
  end

  def going
    instance_options[:going]
  end
end
