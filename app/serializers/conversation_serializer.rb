class ConversationSerializer < ActiveModel::Serializer
  attributes :id, :sender_id, :recipient_id, :recent_message, :updated_at, :user_details, :unread_messages_count, :sender_photo, :recipient_photo

  def user_details
    if scope[:current_user] == object.sender
      ConversationUserSerializer.new(object.recipient , {root: false})
    else
      ConversationUserSerializer.new(object.sender , {root: false})
    end
  end

  def unread_messages_count
    object.messages.unread(scope[:current_user].id).count
  end

  def recipient_photo
    return if object.recipient.blank?

    photo_obj = {}
    missing_pic = object.recipient.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.recipient.photo.present?
      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.recipient.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end

  def sender_photo
    photo_obj = {}
    missing_pic = object.sender.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.sender.photo.present?
      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.sender.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end
end
