class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :comment, :rated_id, :rating, :rater_name

  def rater_name
    object.user.username
  end
end
