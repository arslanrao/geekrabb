class UserListSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :account_type, :user_name, :jid, :city, :education_level, :major, :job, :teaching_modules, :hobbies, :other_major, :photo, :latitude, :longitude, :going

  def photo
    photo_obj = {}
    missing_pic = object.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.photo.present?

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end

  def going
    instance_options[:going]
  end

end
