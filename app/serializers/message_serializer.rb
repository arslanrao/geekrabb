class MessageSerializer < ActiveModel::Serializer
  attributes :id, :body, :conversation_id, :sender_id, :sender_name, :created_at, :photo

  def sender_id
    object.user.id
  end

  def sender_name
    object.user.username
  end

  def photo
    photo_obj = {}
    missing_pic = object.user.male? ? 'profile-image-male.png' : 'profile-image-female.png'

    if object.user.photo.present?

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, object.user.photo.avatar.url(size)].join()
      end
    else

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, "/images/missing/#{size}/", missing_pic].join()
      end
    end

    photo_obj
  end
end
