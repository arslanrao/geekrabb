ActiveAdmin.register VersionControl do
  permit_params :ios_version, :android_version, :ios_enforced, :android_enforced

  index do
    selectable_column
    id_column
    column :ios_version
    column :android_version
    column :ios_enforced
    column :android_enforced
    actions
  end

  show do
    attributes_table do
      row :ios_version
      row :android_version
      row :ios_enforced
      row :android_enforced
    end
  end

  filter :ios_version
  filter :android_version
  filter :ios_enforced
  filter :android_enforced

  form do |f|
    f.inputs do
      f.input :ios_version
      f.input :android_version
      f.input :ios_enforced
      f.input :android_enforced
    end
    f.actions
  end

end
