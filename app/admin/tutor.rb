ActiveAdmin.register User, as: 'Tutors' do

  permit_params :first_name, :last_name, :email, :status

  scope :tutors, default: true


  actions :index, :show, :update, :edit, :destroy

  controller do
    def update
      if resource.update(permitted_params[:user])
        flash[:notice] = "Profile updated successfully"
        redirect_to admin_tutor_path
      else
        flash.now[:alert] = resource.errors.full_messages.to_sentence
        render :edit
      end
    end
  end

  filter :first_name
  filter :last_name
  filter :email
  filter :degree
  filter :gender
  filter :date_of_birth
  filter :city
  filter :institution
  filter :hourly_rate
  filter :teaching_interests
  filter :interested_in_online_teachings
  filter :interested_in_online_lessons
  filter :status

  index do
    selectable_column
    column :first_name
    column :last_name
    column :email
    column :city
    column 'Photo' do |user|
      ul do
        user.degrees.each do |degree|
          li do
            image_tag(degree.avatar.url(:thumb))
          end
        end
      end
    end
    column :status
    column :created_at
    actions
  end

  show do |user|
    attributes_table do
      row :first_name
      row :last_name
      row :account_type
      row :gender
      row :date_of_birth
      row :city
      row :education_level
      row :institution
      row :hourly_rate
      row :status
      row :created_at
      row :updated_at
      row :email
      row :nationality
      row :teaching_interests
      row :languages
      row :teaching_modules
      row :interested_in_online_teachings
      row :interested_in_online_lessons
      row "Photo" do
        ul do
          user.degrees.each do |degree|
            li do
              image_tag(degree.avatar.url(:thumb))
            end
          end
        end
      end
    end
  end

  form do |f|
    f.inputs 'Tutor Details' do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :status
    end
    f.actions
  end

end
