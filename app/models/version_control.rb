class VersionControl < ApplicationRecord

  validates_length_of :ios_version, :android_version, maximum: 25
  validates :ios_enforced, :android_enforced, inclusion: { in: [true, false] }

  private

  def self.validate_version(device_type, version_number)
    return true if version_number.blank? || device_type.blank?
    app_version = self.last

    if app_version.android_enforced && device_type == 'Android' && Gem::Version.new(version_number) < Gem::Version.new(app_version.android_version)
      return false
    elsif app_version.ios_enforced && device_type == 'IOS' && Gem::Version.new(version_number) < Gem::Version.new(app_version.ios_version)
      return false
    end

    return true
  end
end
