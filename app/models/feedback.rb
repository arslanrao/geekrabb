class Feedback < ApplicationRecord
  belongs_to :user
  belongs_to :ratee, class_name: 'User', foreign_key: 'rated_id'

  validates :comment, :rated_id, :rating, presence: true

  after_create :update_user_rating

  private

  def update_user_rating
    user = self.ratee
    ratings = user.ratings
    average_rating = ratings.sum(:rating)/ratings.count

    user.update(rating: average_rating)
  end
end
