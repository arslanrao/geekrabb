class Payment < ApplicationRecord
  belongs_to :user

  PAYMENT_TYPES = ['Paypal', 'Paytab']

  scope :successfull, -> { where("(payment_type = ? AND response_code = ?) OR (payment_type = ? AND mobile_payment = ?) OR (payment_type = ? AND mobile_payment = ? AND state = ?)", 'Paytab', '100', 'Paypal', false, 'Paypal', true, 'approved') }

  validates :payment_type, inclusion: {in: PAYMENT_TYPES}

  validates :transaction_id, :user_id, :response_code, :response_message, :transaction_amount, :transaction_currency, :last_4_digits, :first_4_digits, :card_brand, :trans_date, :secure_sign, presence: true, if: -> { self.paytab? }

  validates :paymentToken, :payerID, :paymentID, :intent, :returnUrl, presence: true, if: -> { self.paypal? }

  validates :state, :paymentID, presence: true, if: -> { self.mobile_payment? }

  def paytab?
    self.payment_type == 'Paytab'
  end

  def paypal?
    self.payment_type == 'Paypal'
  end
end
