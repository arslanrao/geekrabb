class Conversation < ApplicationRecord
  belongs_to :sender, foreign_key: :sender_id, class_name: 'User'
  belongs_to :recipient, foreign_key: :recipient_id, class_name: 'User'

  has_many :messages, dependent: :destroy

  validates :sender, :recipient_id, presence: true
  validates_uniqueness_of :sender_id, scope: :recipient_id
  validate :validate_user_existence

  scope :involving, -> (user) do
    where("conversations.sender_id =? OR conversations.recipient_id =?", user.id, user.id)
  end

  scope :between, -> (sender_id,recipient_id) do
    where("(conversations.sender_id = ? AND conversations.recipient_id =?) OR (conversations.sender_id = ? AND conversations.recipient_id =?)", sender_id, recipient_id, recipient_id, sender_id)
  end

  def mark_messages_read(message_id, user_id)
    self.messages.unread(user_id).where('id <= ?', message_id).update_all(read: true)
  end

  private

  def validate_user_existence
    errors.add(:recipient_id, "is not referenced to any user in the database") unless User.exists?(self.recipient_id)
    errors.add(:sender_id, "is not referenced to any user in the database") unless User.exists?(self.sender_id)
  end
end
