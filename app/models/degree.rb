class Degree < ApplicationRecord
  belongs_to :user

  has_attached_file :avatar, styles: { large: "600x600>", medium: "300x300>", thumb: "100x100>", profile: "300x300#" }, default_url: "/images/missing/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: ["application/pdf", /\Aimage\/.*\z/]

  #validates :avatar, attachment_presence: true
  #validates_with AttachmentPresenceValidator, attributes: :avatar
  #validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 5.megabytes

  validates :name, :institution, presence: true
  validates :other_institute, presence: true, if: -> { self.institution == 'Other' }

end
