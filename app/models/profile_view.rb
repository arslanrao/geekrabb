class ProfileView < ApplicationRecord

  belongs_to :user

  validates_presence_of :user_viewed_id, :user_id

  default_scope { order(updated_at: :desc) }
  scope :this_month, -> { where(updated_at: DateTime.now.beginning_of_month..DateTime.now.end_of_month) }
  scope :recents, -> { where("DATE(updated_at) > ?", (Date.today).to_time - 14.days) }

end
