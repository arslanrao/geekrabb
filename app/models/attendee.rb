class Attendee < ApplicationRecord
  include PushNotification::PushNotificationHelper

  belongs_to :user
  belongs_to :activity

  validates :user_id, presence: true
  validates :going, inclusion: [true, false]
  validate :valid_user?

  after_create :send_notifications, if: :going

  def valid_user?
    unless User.exists?(self.user_id)
      errors.add(:user_id, "is invalid")
    end
  end

  private

  def send_ios_notification_activity recepient_id
    tokens = UserIdentity.where('user_id = ? and device_type = ? and device_token is not null', recepient_id, 'IOS').pluck(:device_token)

    return if tokens.blank?
    data = {message: {title: "#{self.user.username} has joined activity", body: self.activity.name}, type: NOTIFICATION_TYPES[:activity_joined], activity_id: self.activity.id}
    send_ios_notification(data, 'activity_joined', tokens)
  end

  def send_android_notification_activity recepient_id
    tokens = UserIdentity.where('user_id = ? and device_type = ? and device_token is not null', recepient_id, 'Android').pluck(:device_token)

    return if tokens.blank?
    data = {message: "#{self.user.username} has joined activity: #{self.activity.name}", key: NOTIFICATION_TYPES[:activity_joined], activity_id: self.activity.id, detail: self.activity.name}
    options = {data: data,  collapse_key: "activity_joined", priority: "high", notification: data}
    send_android_notification(options, tokens)
  end

  def send_notifications
    user = self.activity&.user
    return if user.blank?
    recepient_id = user.id
    send_ios_notification_activity(recepient_id)
    send_android_notification_activity(recepient_id)
  end
end
