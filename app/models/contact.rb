class Contact < ApplicationRecord
  validates :email, :message, presence: true

  after_create :contact_message_to_admin

  private

  def contact_message_to_admin
    UserMailer.contact_us(email, subject, message).deliver
  end
end
