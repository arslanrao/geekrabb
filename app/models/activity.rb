class Activity < ApplicationRecord
  include PushNotification::PushNotificationHelper

  PER_PAGE = 25

  attr_accessor :device_id

  has_many :attendees, dependent: :destroy
  has_many :user, through: :attendees
  belongs_to :user

  accepts_nested_attributes_for :attendees, allow_destroy: true

  validates :name, :detail, :starting_time, :location, presence: true
  validates :name, length: { maximum: 255 }
  validates :gender, inclusion: { in: ['Male', 'Female', 'Both'] }
  #validate :valid_starting_time?

  after_create :send_notifications

  geocoded_by :location
  after_validation :geocode

  def valid_starting_time?
    if starting_time.present? && starting_time < Time.now
      errors.add(:starting_time, "should be in future")
    end
  end

  private

  def send_ios_notification_activity recepient_ids
    tokens = UserIdentity.where('user_id in (?) and device_type = ? and device_token is not null', recepient_ids, 'IOS').pluck(:device_token)
    data = {message: {title: self.user.username, body: "#{self.user.username} created #{self.name} activity"}, type: NOTIFICATION_TYPES[:activity], id: self.id, sender_name: self.user.username}
    send_ios_notification(data, 'activity_created', tokens)
  end

  def send_android_notification_activity recepient_ids
    return true if self.device_id.blank?
    tokens = UserIdentity.where('user_id in (?) and device_type = ? and device_token is not null', recepient_ids, 'Android').pluck(:device_token)
    data = {message: "#{self.user.username} created #{self.name} activity", sender_name: self.user.username, key: NOTIFICATION_TYPES[:activity], user_id: self.user_id, detail: self.detail, id: self.id}
    options = {data: data,  collapse_key: "activity_created", priority: "high", notification: data}
    send_android_notification(options, tokens)
  end

  def send_notifications
    where = ""
    where +=  " and (#{age_range_conditions()})" if self.age_range.present?
    where += " and gender = '#{self.gender}'" if self.gender && self.gender.downcase != 'both'

    recepient_ids = User.where("hobbies @> ? #{where}", "{#{self.name}}").pluck(:id)

    send_ios_notification_activity(recepient_ids)
    send_android_notification_activity(recepient_ids)
  end

  def age_range_conditions
    age_conditions = ''

    self.age_range.each_with_index do |age_range, index|
      ranged_data = age_range.split('-')

      age_conditions += ' or ' if index > 0 

      if ranged_datself.count > 1
        age_conditions += "date_part('year', age(date_of_birth)) >= #{ranged_data[0]}"
        age_conditions += " and date_part('year', age(date_of_birth)) <= #{ranged_data[1]}"
      else
        ranged_data = age_range.split('+')
        age_conditions += "date_part('year', age(date_of_birth)) >= #{ranged_data[0]}"
      end
    end

    age_conditions
  end
end
