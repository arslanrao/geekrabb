class Message < ApplicationRecord
  include PushNotification::PushNotificationHelper

  belongs_to :conversation
  belongs_to :user

  attr_accessor :device_id

  validates_presence_of :body, :conversation_id, :user_id
  validate :validate_user_id_with_conversation_id
  scope :latest_to, -> (message_id) { where("id > ?", message_id) }
  scope :unread, -> (user_id) { where('read = ? and user_id != ?', false, user_id) }

  after_create :update_recent_message
  after_create :send_ios_notification_message
  after_create :send_android_notification_message

  private

  def update_recent_message
    unless conversation.update(recent_message: body)
      errors.add(:recent_message, 'Failed to update recent message')
      raise ActiveRecord::Rollback
    end
  end

  def send_ios_notification_message
    return true if self.device_id.blank?
    recipient_id = conversation.sender_id == user_id ? conversation.recipient_id : conversation.sender_id
    tokens = UserIdentity.where('user_id = ? and device_type = ? and device_token is not null', recipient_id, 'IOS').pluck(:device_token)

    return true if tokens.blank?
    data = {message: {title: self.user.username, body: self.body}, conversation: self.conversation_id, sender_name: self.user.username, type: NOTIFICATION_TYPES[:message]}
    send_ios_notification(data, 'message_created', tokens)
  end

  def send_android_notification_message
    return true if self.device_id.blank?
    recipient_id = conversation.sender_id == user_id ? conversation.recipient_id : conversation.sender_id
    tokens = UserIdentity.where('user_id = ? and device_type = ? and device_token is not null', recipient_id, 'Android').pluck(:device_token)

    return true if tokens.blank?
    data = {message: self.body, sender_name: self.user.username, key: 'message_created', user_id: self.user_id, conversation: self.conversation_id}
    options = {data: data,  collapse_key: "message_created", priority: "high", notification: data}
    send_android_notification(options, tokens)
  end

  def validate_user_id_with_conversation_id
    conversation = Conversation.find(self.conversation_id)
    unless conversation.present? || conversation.sender_id == self.user_id
      errors.add(:user_id, "is not valid for this conversation")
    end
  end
end
