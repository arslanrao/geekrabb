class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  PER_PAGE = 25
  MAX_USERS_CONTACT_INFO_VIEWS = 3

  ACCOUNT_TYPES = ['Student', 'Tutor', 'Employee/Employer', 'Hobby']

  before_create :deactivate_tutor
  before_validation :setJid
  after_create :send_welcome_email

  has_many :user_identities, dependent: :destroy
  has_many :degrees, dependent: :destroy
  has_many :conversations, foreign_key: :sender_id, dependent: :destroy
  has_many :payments
  has_one :photo, as: :attachable, dependent: :destroy
  has_many :feedbacks, dependent: :destroy
  has_many :profile_views, dependent: :destroy
  has_many :attendees, dependent: :destroy
  has_many :activities, through: :attendees
  has_many :ratings, class_name: 'Feedback', foreign_key: 'rated_id', dependent: :destroy

  acts_as_followable
  acts_as_follower

  scope :tutors, -> { where(account_type: 'Tutor') }
  scope :students, -> { where(account_type: 'Student') }
  scope :employees, -> { where(account_type: 'Employee/Employer') }
  scope :hobbyists, -> { where(account_type: 'Hobby') }

  validates :first_name, :last_name, :email, :user_name, :account_type, :city, presence: true
  validates_uniqueness_of :user_name, :jid
  validates :account_type, inclusion: { in: ACCOUNT_TYPES }
  validates :gender, :education_level, :institution, :major, :mobile_number, presence: true, if: -> { self.account_type == 'Student' }
  validates_length_of :institution, :major, minimum: 1, maximum: 250, if: -> { self.account_type == 'Student' }

  validates :gender, :education_level, :nationality, :teaching_modules, :work_experience, presence: true, if: -> { self.account_type == 'Tutor' }
  validates :hourly_rate, numericality: { only_float: true }, if: -> { self.account_type == 'Tutor' }

  validate :date_of_birth_validness
  validates :date_of_birth, :job, :mobile_number, presence: true, if: -> { self.account_type == 'Employee/Employer' }
  validates :date_of_birth, :hobbies, :mobile_number, presence: true, if: -> { self.account_type == 'Hobby' }

  validates :other_institute, presence: true, if: -> { self.institution == 'Other' }
  validates :other_major, presence: true, if: -> { self.major == 'Other' }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates_length_of :first_name, :last_name, :email, minimum: 1, maximum: 250
  validates_numericality_of :mobile_number, only_integer: true, allow_nil: true


  reverse_geocoded_by :latitude, :longitude
  after_validation :reverse_geocode

  accepts_nested_attributes_for :degrees, allow_destroy: true, reject_if: proc { |attributes| attributes['name'].blank? || attributes['institution'].blank? }
  accepts_nested_attributes_for :photo, allow_destroy: true

  ransacker :teaching_modules do
    Arel.sql("array_to_string(teaching_modules, ',')")
  end

  ransacker :hobbies do
    Arel.sql("array_to_string(hobbies, ',')")
  end

  def female?
    self.gender == 'Female'
  end

  def male?
    self.gender == 'Male'
  end

  def student?
    self.account_type == 'Student'
  end

  def tutor?
    self.account_type == 'Tutor'
  end

  def employee?
    self.account_type == 'Employee/Employer'
  end

  def hobby?
    self.account_type == 'Hobby'
  end

  def generate_access_token(device_id, device_type)
    user_identity = user_identities.find_or_create_by(device_id: device_id, device_type: device_type)
    user_identity.access_token
  end

  def username
    [first_name, last_name].join(' ')
  end

  def active_for_authentication?
    if self.tutor?
      super and self.status?
    else
      super
    end
  end

  def related_conversations
    Conversation.where('(sender_id = ? or recipient_id = ?) and recent_message is not null', self.id, self.id)
  end

  def avatar_url
    profile_photo = self.photo
    [ActionController::Base.asset_host, profile_photo.avatar.url(:profile)].join() if profile_photo && profile_photo.avatar.present?
  end

  def convo_with_unread_mesg
    self.related_conversations.joins(:messages).where('messages.read = ? and messages.user_id != ?', false, self.id).distinct
  end

  def recent_related_conversations
    self.related_conversations.includes(:messages).sort_by{ |c| - c.messages.last&.id.to_i }
  end

  def date_of_birth_validness dob = nil
    dob ||= self.date_of_birth

    begin
       Date.parse(dob.to_s) if dob.present?
    rescue ArgumentError
       self.errors.add(:date_of_birth, 'Invalid date of birth')
    end
  end

  def create_openfire_user user_password
    user_data = {
      "openfire": {
        "username": self.jid,
        "password": user_password,
        "name": self.username,
        "email": self.email,
        },
      "vcard_data": {
        "username": self.jid,
        "image_url": self.image_for_chat,
        "first_name": self.first_name,
        "last_name": self.last_name,
        "user_id": self.id
      }
    }
    client = OpenfireClient.new
    client.create_user(user_data)
  end

  def update_openfire_user user_password = nil
    user_data = {
      "password": user_password,
      "vcard_data": {
        "username": self.jid,
        "image_url": self.image_for_chat,
        "first_name": self.first_name,
        "last_name": self.last_name,
        "user_id": self.id
      }
    }
    client = OpenfireClient.new
    client.update_user(user_data)
  end

  def image_for_chat
    image = if self.photo.present?
      self.photo.avatar.url(:profile)
    elsif self.male?
      'prifile-image-male.png'
    elsif self.female?
      'prifile-image-female.png'
    else
      'prifile-image.jpg'
    end

    ActionController::Base.helpers.asset_path(image)
  end

  def reset_pin!
    self.update_column(:pin, rand(1000..9999))
  end

  def unverify!
    self.update_column(:verified, false)
  end

  def send_pin!
    reset_pin!
    unverify!

    client = Nexmo::Client.new(api_key: APP_CONFIG[:sms][:key], api_secret: APP_CONFIG[:sms][:secret])
    client.sms.send(from: 'Geekrabb', to: self.mobile_number, text: "Geekrabb code: #{self.pin}. Valid for 60 minutes.")

    self.touch(:pin_sent_at)
  end

  private
  def deactivate_tutor
    self.status = false if self.tutor?
  end

  def send_welcome_email
    UserMailer.welcome_email(self)
  end

  def setJid
    self.jid = self.user_name.gsub(/[^a-zA-Z0-9\-]/,"")
  end

end
