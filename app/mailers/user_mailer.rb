class UserMailer < ApplicationMailer
  default from: 'noreply@geekrabb.com'

  def welcome_email(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome to Geekrabb')
  end

  def contact_us(email, subject, message)
    @user_email = email
    @subject = subject
    @message = message
    mail(to: ['bilalkhaan@hotmail.com', 'khurram309@gmail.com'], subject: 'Geekrabb contact request')
  end
end
