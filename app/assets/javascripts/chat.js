/**
 * Chat logic
 *
 * Most of the js functionality is inspired from anatgarg.com
 * jQuery tag Module from the tutorial
 * http://anantgarg.com/2009/05/13/gmail-facebook-style-jquery-chat/
 *
 */


var chatboxFocus = new Array();
var chatBoxes = new Array();
var overloadedChatBoxes = new Array();

loadedMessages = {};
messageCountPerUser = {};

renderMessages = function(conversation_id, first_name, receiver_image, count) {
    for (var i = loadedMessages[conversation_id].length - 1; i >= 0; i--) {
        if(loadedMessages[conversation_id][i]["from"] ==  (conversation_id + $('#chat_url').val())) {
            message_class = 'other';
            name = first_name;
            image = receiver_image;
        } else {
            message_class = 'self';
            name = $('#firstname').val();
            image = $("#user_image").val();
        }
        var message_content = "<li id='added-before' class="+ message_class +">" +
            "<div class='avatar'>" +
              "<img src=" + image + ">" +
            "</div>" +
            "<div class='chatboxmessagecontent'>" +
              "<p>" + loadedMessages[conversation_id][i]["body"] + "</p>" +
              "<time title='" + format_date(loadedMessages[conversation_id][i]["stamp"]) + "'>" +
                "<p>" + name + " • " + (format_time(loadedMessages[conversation_id][i]["stamp"])) +"</p>" +
              "</time>" +
            "</div>" +
            "</li>";
        $("#chatbox_" + conversation_id + " .chatboxcontent").prepend(message_content);
    }

    if (messageCountPerUser[conversation_id]){
        messageCountPerUser[conversation_id]['count'] += loadedMessages[conversation_id].length;
    }
    else {
        messageCountPerUser[conversation_id] = {};
        messageCountPerUser[conversation_id]['count'] = loadedMessages[conversation_id].length;
    }

    messageCountPerUser[conversation_id]['total'] = count;

    console.log("updated count");
    console.log(messageCountPerUser[conversation_id]);

    loadedMessages[conversation_id] = [];
}


retrieveMessages = function(jid, first_name, receiver_image, action) {
    before = ""
    conversation_id = jid.split('@')[0];
    loadedMessages[conversation_id] = [];

    if ($("#chatbox_" + conversation_id + " .chatboxcontent").data('before')) {
        before = $("#chatbox_" + conversation_id + " .chatboxcontent").data('before');
    }

    console.log("before ==== " + before);
    if (before == "" || messageCountPerUser[conversation_id]['total'] > messageCountPerUser[conversation_id]['count']) {
        conn.mam.query(conn.jid.split('/')[0], {
          "with": jid,
          "max": '20',
          "before": before,
          onMessage: function(message) {
            messageData = {};
            messageData["from"] = $(message).find("forwarded message").attr("from").split('/')[0];
            messageData["body"] = $(message).find("forwarded message body").text();
            messageData["stamp"] = $(message).find("forwarded delay").attr("stamp");
            loadedMessages[conversation_id].push(messageData);
            return true;
          },
          onComplete: function(response) {
            $("#chatbox_" + conversation_id + " .chatboxcontent").off('scroll')
            $("#chatbox_" + conversation_id + " .chatboxcontent").on('scroll', function(e){
                if ($(this).scrollTop() == 0 && loadedMessages[$(this).parent().attr('id').split("_")[1]].length == 0) {
                    if (messageCountPerUser[conversation_id]['count'] < parseInt($(response).find('count').text())) {
                        var jid = $(this).parent().attr('id').split("_")[1] + $('#chat_url').val();
                        $("#chatbox_" + $(this).parent().attr('id').split ("_")[1] + " .chatboxcontent .chat-loader").removeClass('hidden');
                        retrieveMessages(jid, first_name, receiver_image, null);
                    }
                }
            });
            console.log("Got all the messages");
            // if (messageCountPerUser[conversation_id]['count'] < parseInt($(response).find('count').text())) {
                var scrollTo = $("#chatbox_" + conversation_id + " .chatboxcontent")[0].scrollHeight;

                if ($("#chatbox_" + conversation_id + " .chatboxcontent li").length > 0)
                    scrollTo = $("#chatbox_" + conversation_id + " .chatboxcontent li").first()[0].scrollHeight;
                console.log(parseInt($(response).find('count').text()));
                
                renderMessages(conversation_id, first_name, receiver_image, parseInt($(response).find('count').text()));

                if (action == "initial")
                    $("#chatbox_" + conversation_id + " .chatboxcontent").scrollTop($("#chatbox_" + conversation_id + " .chatboxcontent")[0].scrollHeight);
                else
                    $("#chatbox_" + conversation_id + " .chatboxcontent").scrollTop(scrollTo);
            // }
            $("#chatbox_" + conversation_id + " .chatboxcontent .chat-loader").addClass('hidden');
            $("#chatbox_" + conversation_id + " .chatboxcontent").data('before', $(response).find('first').text())
          }
        });
    }
}


collapse_chat_boxes = function(totalChatBoxes, firstCharBox) {
    if(totalChatBoxes + 1 > 4) {
        chatBox.close(firstCharBox);
        var width = (totalChatBoxes) * (280 + 7) + 20;

        if ($('#chat-list').length == 0) {
            var chat_list = $("<ul id='chat-list'></ul>");
            chat_list.addClass('hidden');
            chat_list.css('right', width + 'px');
            chat_list.css('bottom', '45px');
            $("body").append(chat_list);

            $('body').on('click', '#chat-list li', function(e) {
                console.log(this.id);
                e.preventDefault();

                index = $.inArray(this.id, overloadedChatBoxes)
                if(index > -1) {
                    overloadedChatBoxes.splice(index, 1)
                    var target_chat = this.id;
                    $('#chat-list').find("#" + this.id).remove();

                    for (x in chatBoxes) {
                        if ($("#chatbox_" + chatBoxes[x]).css('display') != 'none') {
                            $('#chatbox_' + target_chat).css('display', 'block');
                            $('#chatbox_' + target_chat + ' .chatboxinput').css('display', 'block');
                            $("#chatbox_" + target_chat + " .chatboxcontent").css('display', 'block');
                            console.log(chatBoxes[x]);
                            overloadedChatBoxes.push(chatBoxes[x]);
                            $('#chat-list').append("<li id="+ chatBoxes[x] +"><a href='#'>" + chatBoxes[x] + "</a></li>");
                            chatBox.close(chatBoxes[x]);

                            break;
                        }
                    }
                }
            })
        }

        if ($('#show-more').length == 0) {
            var moreBtn = $("<button id='show-more' class='show-more'><span id='more-chats'>more</span></button>");
            moreBtn.css('right', width + 'px');
            $("body").append(moreBtn);
        }
        $('#show-more').removeClass('hidden');
        overloadedChatBoxes.push(firstCharBox);
        $('#chat-list').append("<li id="+ firstCharBox +"><a href='#'>" + firstCharBox + "</a></li>");
    }
}

$(document).on('click', '#show-more', function() {
    if ($('#chat-list').hasClass('hidden')) {
        $('#chat-list').removeClass('hidden');
    } else {
        $('#chat-list').addClass('hidden');
    }
    
})

format_date = function(dateObject) {
    var d = new Date(dateObject);
    var minutes = d.getMinutes();
    var hours = d.getHours()
    var day = d.getDate();

    var locale = "en-us";
    var month = d.toLocaleString(locale, {month: "short"});
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + ' ' + month + ' ' + year + ' at ' + hours + ':' + minutes;

    return date;
};

format_time = function(dateObject) {
  var d = new Date(dateObject);
  var minutes = d.getMinutes();
  var hours = d.getHours();

  return  hours + ':' + minutes;
}

list_messages = function(messages, conversation_id, first_name, receiver_image, jid) {
    for(var k = 0; k < messages.length; k++) {
        if(messages[k].from ==  jid) {
            message_class = 'other';
            name = first_name;
            image = receiver_image;
        } else {
            message_class = 'self';
            name = $('#firstname').val();
            image = $("#user_image").val();
        }
      var message_content = "<li class="+ message_class +">" +
        "<div class='avatar'>" +
          "<img src=" + image + ">" +
        "</div>" +
        "<div class='chatboxmessagecontent'>" +
          "<p>" + messages[k].body + "</p>" +
          "<time title='" + format_date(messages[k].timestamp) + "'>" +
            "<p>" + name + " • " + (format_time(messages[k].timestamp)) +"</p>" +
          "</time>" +
        "</div>" +
      "</li>";
      $("#chatbox_" + conversation_id + " .chatboxcontent").append(message_content);
  }
  $("#chatbox_" + conversation_id + " .chatboxcontent").scrollTop($("#chatbox_" + conversation_id + " .chatboxcontent")[0].scrollHeight);
}

var ready = function () {

    chatBox = {

        /**
         * creates an inline chatbox on the page by calling the
         * createChatBox function passing along the unique conversation_id
         *
         * @param conversation_id
         */

        chatWith: function (conversation_id, message) {

            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/user_info_for_chat?jid=" + conversation_id,
                dataType: "json",
                success: function (result) {
                    chatBox.createChatBox(conversation_id, result['first_name'], result['image_url'], message);
                    $("#chatbox_" + conversation_id + " .chatboxtextarea").focus();
                },
                error: function (){
                    console.log("Unable to chat :(");
                }
            });
        },

        /**
         * closes the chatbox by essentially hiding it from the page
         *
         * @param conversation_id
         */

        close: function (conversation_id) {
            $('#chatbox_' + conversation_id).css('display', 'none');
            chatBox.restructure();
        },

        adjustBoxesOnClose: function () {
            if (overloadedChatBoxes.length > 0) {
                var id = overloadedChatBoxes.pop();
                $('#chatbox_' + id).css('display', 'block');
                $('#chatbox_' + id + ' .chatboxinput').css('display', 'block');
                $("#chatbox_" + id + " .chatboxcontent").css('display', 'block');

                $('li #' + id).remove();
            }

            if(overloadedChatBoxes.length == 0) {
                $('#chat-list').addClass('hidden');
                $('#show-more').addClass('hidden');
            }
        },

        /**
         * Plays a notification sound when a new chat message arrives
         */

        notify: function () {
            var audioplayer = $('#chatAudio')[0];
            audioplayer.play();
        },

        /**
         * Handles 'smart layouts' of the chatboxes. Like when new chatboxes are
         * added or removed from the view, it restructures them so that they appear
         * neatly aligned on the page
         */

        restructure: function () {
            align = 0;
            for (x in chatBoxes) {
                chatbox_id = chatBoxes[x];

                if ($("#chatbox_" + chatbox_id).css('display') != 'none') {
                    if (align == 0) {
                        $("#chatbox_" + chatbox_id).css('right', '20px');
                    } else {
                        width = (align) * (280 + 7) + 20;
                        $("#chatbox_" + chatbox_id).css('right', width + 'px');
                    }
                    align++;
                }
            }

        },

        /**
         * Takes in two parameters. It is responsible for fetching the specific conversation's
         * html page and appending it to the body of our home page e.g if conversation_id = 1
         *
         * $.get("conversations/1, function(data){
         *    // rest of the logic here
         * }, "html")
         *
         * @param conversation_id
         * @param minimizeChatBox
         */

        createChatBox: function (conversation_id, first_name, receiver_image, message,  minimizeChatBox) {
            if ($("#chatbox_" + conversation_id).length > 0) {
                if ($("#chatbox_" + conversation_id).css('display') == 'none') {
                    $("#chatbox_" + conversation_id).css('display', 'block');
                    chatBox.restructure();
                }
                $("#chatbox_" + conversation_id + " .chatboxtextarea").focus();
                return;
            }

            $("body").append('<div id="chatbox_' + conversation_id + '" class="chatbox"></div>')

            chat_data = "<div class='chatboxhead'>" +
              "<div class='chatboxtitle'>" +
                "<i class='fa fa-comments'></i>" +
                "<h1>" + first_name + "</h1>" +
              "</div>" +
              "<div class='chatboxoptions'>" +
                "<a class='toggleChatBox' data-cid="+conversation_id+"><i class='fa fa-minus'></i></a>" +
                "<a class='closeChat' data-cid="+conversation_id+"><i class='fa fa-times'></i></a>" +
              "</div>" +
              "<br clear='all'/>" +
            "</div>" +
            "<div class='chatboxcontent'>" +
                "<div class='chat-loader hidden'>" +
                    "<img src='/images/chat_loader.gif' />" +
                "</div>" +
            "</div>" +
            "<p id='is-typing' class='hidden'>" + first_name + " is typing ...</p>" +
            "<div class='chatboxinput'>" +
              "<textarea class='chatboxtextarea' data-cid="+conversation_id+"></textarea>" +
            "</div>";

            $('#chatbox_' + conversation_id).html(chat_data);

            var cachedResponseRsm = null;

            var jid = conversation_id + $('#chat_url').val();
            retrieveMessages(jid, first_name, receiver_image, "initial");

            $("#chatbox_" + conversation_id).css('bottom', '0px');

            chatBoxeslength = 0;
            firstCharBox = null;

            for (x in chatBoxes) {
                if ($("#chatbox_" + chatBoxes[x]).css('display') != 'none') {
                    if(!firstCharBox)
                        firstCharBox = chatBoxes[x];
                    chatBoxeslength++;
                }
            }

            if (chatBoxeslength == 0) {
                $("#chatbox_" + conversation_id).css('right', '20px');
            } else {
                width = (chatBoxeslength) * (280 + 7) + 20;
                $("#chatbox_" + conversation_id).css('right', width + 'px');
            }

            chatBoxes.push(conversation_id);

            if (minimizeChatBox == 1) {
                minimizedChatBoxes = new Array();

                if ($.cookie('chatbox_minimized')) {
                    minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
                }
                minimize = 0;
                for (j = 0; j < minimizedChatBoxes.length; j++) {
                    if (minimizedChatBoxes[j] == conversation_id) {
                        minimize = 1;
                    }
                }

                if (minimize == 1) {
                    $('#chatbox_' + conversation_id + ' .chatboxcontent').css('display', 'none');
                    $('#chatbox_' + conversation_id + ' .chatboxinput').css('display', 'none');
                }
            }

            chatboxFocus[conversation_id] = false;

            $("#chatbox_" + conversation_id + " .chatboxtextarea").blur(function () {
                chatboxFocus[conversation_id] = false;
                $("#chatbox_" + conversation_id + " .chatboxtextarea").removeClass('chatboxtextareaselected');
            }).focus(function () {
                chatboxFocus[conversation_id] = true;
                $('#chatbox_' + conversation_id + ' .chatboxhead').removeClass('chatboxblink');
                $("#chatbox_" + conversation_id + " .chatboxtextarea").addClass('chatboxtextareaselected');
            });

            $("#chatbox_" + conversation_id).click(function () {
                if ($('#chatbox_' + conversation_id + ' .chatboxcontent').css('display') != 'none') {
                    $("#chatbox_" + conversation_id + " .chatboxtextarea").focus();
                }
            });

            $("#chatbox_" + conversation_id).show();

            collapse_chat_boxes(chatBoxeslength, firstCharBox);

        },

        /**
         * Responsible for listening to the keypresses when chatting. If the Enter button is pressed,
         * we submit our conversation form to our rails app
         *
         * @param event
         * @param chatboxtextarea
         * @param conversation_id
         */

        checkInputKey: function (event, chatboxtextarea, conversation_id) {
            if (event.keyCode == 13 && event.shiftKey == 0) {
                event.preventDefault();

                message = chatboxtextarea.val();
                message = message.replace(/^\s+|\s+$/g, "");

                if (message != '') {
                    $('#conversation_form_' + conversation_id).submit();
                    $(chatboxtextarea).val('');
                    $(chatboxtextarea).focus();
                    $(chatboxtextarea).css('height', '44px');
                }
            }

            var adjustedHeight = chatboxtextarea.clientHeight;
            var maxHeight = 94;

            if (maxHeight > adjustedHeight) {
                adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
                if (maxHeight)
                    adjustedHeight = Math.min(maxHeight, adjustedHeight);
                if (adjustedHeight > chatboxtextarea.clientHeight)
                    $(chatboxtextarea).css('height', adjustedHeight + 8 + 'px');
            } else {
                $(chatboxtextarea).css('overflow', 'auto');
            }

        },

        /**
         * Responsible for handling minimize and maximize of the chatbox
         *
         * @param conversation_id
         */

        toggleChatBoxGrowth: function (conversation_id) {
            if ($('#chatbox_' + conversation_id + ' .chatboxcontent').css('display') == 'none') {

                var minimizedChatBoxes = new Array();

                if ($.cookie('chatbox_minimized')) {
                    minimizedChatBoxes = $.cookie('chatbox_minimized').split(/\|/);
                }

                var newCookie = '';

                for (i = 0; i < minimizedChatBoxes.length; i++) {
                    if (minimizedChatBoxes[i] != conversation_id) {
                        newCookie += conversation_id + '|';
                    }
                }

                newCookie = newCookie.slice(0, -1)


                $.cookie('chatbox_minimized', newCookie);
                $('#chatbox_' + conversation_id + ' .chatboxcontent').css('display', 'block');
                $('#chatbox_' + conversation_id + ' .chatboxinput').css('display', 'block');
                $("#chatbox_" + conversation_id + " .chatboxcontent").scrollTop($("#chatbox_" + conversation_id + " .chatboxcontent")[0].scrollHeight);
            } else {

                var newCookie = conversation_id;

                if ($.cookie('chatbox_minimized')) {
                    newCookie += '|' + $.cookie('chatbox_minimized');
                }


                $.cookie('chatbox_minimized', newCookie);
                $('#chatbox_' + conversation_id + ' .chatboxcontent').css('display', 'none');
                $('#chatbox_' + conversation_id + ' .chatboxinput').css('display', 'none');
            }

        }



    }


    /**
     * Cookie plugin
     *
     * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
     * Dual licensed under the MIT and GPL licenses:
     * http://www.opensource.org/licenses/mit-license.php
     * http://www.gnu.org/licenses/gpl.html
     *
     */

    jQuery.cookie = function (name, value, options) {
        if (typeof value != 'undefined') { // name and value given, set cookie
            options = options || {};
            if (value === null) {
                value = '';
                options.expires = -1;
            }
            var expires = '';
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                var date;
                if (typeof options.expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
            }
            // CAUTION: Needed to parenthesize options.path and options.domain
            // in the following expressions, otherwise they evaluate to undefined
            // in the packed version for some reason...
            var path = options.path ? '; path=' + (options.path) : '';
            var domain = options.domain ? '; domain=' + (options.domain) : '';
            var secure = options.secure ? '; secure' : '';
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
        } else { // only name given, get cookie
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
    };


}

$(document).ready(ready);
$(document).on("page:load", ready);
