(->
  hide_all_fields = ->
    $('#nationality_div').addClass('hidden')
    $('#nationality_field').prop 'disabled', true
    $('#languages_div').addClass('hidden')
    $('#languages_field').prop 'disabled', true
    $('#teaching_modules_div').addClass('hidden')
    $('#teaching_modules_field').prop 'disabled', true
    $('#work_experience_div').addClass('hidden')
    $('#work_experience_field').prop 'disabled', true
    $('#teaching_interests_div').addClass('hidden')
    $('#teaching_interests_field').prop 'disabled', true
    $('#major_div').addClass('hidden')
    $('#major_field').prop 'disabled', true
    $('#institution_div').addClass('hidden')
    $('#institution_field').prop 'disabled', true
    $('#other_institution_div').addClass('hidden')
    $('#other_institution_field').prop 'disabled', true
    $('#other_major_div').addClass('hidden')
    $('#other_major_field').prop 'disabled', true
    $('#gender_div').addClass('hidden')
    $('#gender_field').prop 'disabled', true
    $('#education_level_div').addClass('hidden')
    $('#education_level_field').prop 'disabled', true
    $('#availablity_start_time_div').addClass('hidden')
    $('#availablity_start_time_field').prop 'disabled', true
    $('#availablity_end_time_div').addClass('hidden')
    $('#availablity_end_time_field').prop 'disabled', true
    $('#job_div').addClass('hidden')
    $('#job_field').prop 'disabled', true
    $('#hobbies_div').addClass('hidden')
    $('#hobbies_field').prop 'disabled', true
    $('#hourly_rate_div').addClass('hidden')
    $('#hourly_rate_field').prop 'disabled', true
    $('#degree_div').addClass('hidden')
    $('#user_interested_in_online_lessons').prop 'disabled', true
    $('#interested_in_online_lessons_div').addClass('hidden')
    $('#user_interested_in_online_teachings').prop 'disabled', true
    $('#interested_in_online_teachings_div').addClass('hidden')

  bind_user_account_type = ->
    $('#user_account_type').on 'change', ->
      if $(this).val() == 'Student'
        hide_all_fields()
        $('#gender_div').removeClass('hidden')
        $('#gender_field').prop 'disabled', false
        $('#education_level_div').removeClass('hidden')
        $('#education_level_field').prop 'disabled', false
        $('#availablity_start_time_div').removeClass('hidden')
        $('#availablity_start_time_field').prop 'disabled', false
        $('#availablity_end_time_div').removeClass('hidden')
        $('#availablity_end_time_field').prop 'disabled', false
        $('#major_div').removeClass('hidden')
        $('#major_field').prop 'disabled', false
        $('#institution_div').removeClass('hidden')
        $('#institution_field').prop 'disabled', false
        $('#interested_in_online_lessons_div').removeClass('hidden')
        $('#user_interested_in_online_lessons').prop 'disabled', false
        $('#degree_div').removeClass('hidden')
      else if $(this).val() == 'Tutor'
        hide_all_fields()
        $('#gender_div').removeClass('hidden')
        $('#gender_field').prop 'disabled', false
        $('#education_level_div').removeClass('hidden')
        $('#education_level_field').prop 'disabled', false
        $('#availablity_start_time_div').removeClass('hidden')
        $('#availablity_start_time_field').prop 'disabled', false
        $('#availablity_end_time_div').removeClass('hidden')
        $('#availablity_end_time_field').prop 'disabled', false
        $('#nationality_div').removeClass('hidden')
        $('#nationality_field').prop 'disabled', false
        $('#languages_div').removeClass('hidden')
        $('#languages_field').prop 'disabled', false
        $('#teaching_modules_div').removeClass('hidden')
        $('#teaching_modules_field').prop 'disabled', false
        $('#work_experience_div').removeClass('hidden')
        $('#work_experience_field').prop 'disabled', false
        $('#teaching_interests_div').removeClass('hidden')
        $('#teaching_interests_field').prop 'disabled', false
        $('#hourly_rate_div').removeClass('hidden')
        $('#hourly_rate_field').prop 'disabled', false
        $('#interested_in_online_teachings_div').removeClass('hidden')
        $('#user_interested_in_online_teachings').prop 'disabled', false
        $('#degree_div').removeClass('hidden')
      else if $(this).val() == 'Employee/Employer'
        hide_all_fields()
        $('#job_div').removeClass('hidden')
        $('#job_field').prop 'disabled', false
      else if $(this).val() == 'Hobby'
        hide_all_fields()
        $('#hobbies_div').removeClass('hidden')
        $('#hobbies_field').prop 'disabled', false

  bind_other_institution_name = ->
    $('#institution_field').on 'change', ->
      if $(this).val() == 'Other'
        $('#other_institution_div').removeClass('hidden')
        $('#other_institution_field').prop 'disabled', false
      else
        $('#other_institution_div').addClass('hidden')
        $('#other_institution_field').prop 'disabled', true

  bind_other_major_name = ->
    $('#major_field').on 'change', ->
      if $(this).val() == 'Other'
        $('#other_major_div').removeClass('hidden')
        $('#other_major_field').prop 'disabled', false
      else
        $('#other_major_div').addClass('hidden')
        $('#other_major_field').prop 'disabled', true

  bind_datepicker = ->
    $('.datepicker').datepicker
      format: 'dd/mm/yyyy'
      autoclose: true

  window.Registration or (window.Registration = {})

  Registration.init = ->
    init_controls()

  init_controls = ->
    bind_user_account_type()
    bind_other_institution_name()
    bind_other_major_name()
    bind_datepicker()
    $('.multiple-selected').multiselect enableClickableOptGroups: true
    $('#user_account_type').trigger 'change'

).call this
