var ready = function () {

  function onSubscriptionRequest(stanza) {
    if (stanza.getAttribute("type") == "subscribe") {
      var from = $(stanza).attr('from');
      console.log('onSubscriptionRequest: from=' + from);
      // Send a 'subscribed' notification back to accept the incoming
      // subscription request
      conn.send($pres({
        to: from,
        type: "subscribed"
      }));

      subscribePresence(from);
    }
    return true;
  }

  function subscribePresence(jid) {
    console.log('subscribePresence: ' + jid);
    conn.send($pres({
      to: jid,
      type: "subscribe"
    }));
  }

  $(".sign-in-btn input").click(function() {
    localStorage.setItem("OrangeStack", $("#inputPassword3").val());
  });

  if ($('#user_id').val() != '') {
    var username = $('#user_id').val();
    establish_connection(username);
  }

    /**
     * When the send message link on our home page is clicked
     * send an ajax request to our rails app with the sender_id and
     * recipient_id
     */

    $('.start-conversation').click(function (e) {
        e.preventDefault();
        chatBox.chatWith($(this).data('username'));
    });

    /**
     * Used to minimize the chatbox
     */

    $(document).on('click', '.toggleChatBox', function (e) {
        e.preventDefault();

        var id = $(this).data('cid');
        chatBox.toggleChatBoxGrowth(id);
    });

    /**
     * Used to close the chatbox
     */

    $(document).on('click', '.closeChat', function (e) {
        e.preventDefault();

        var id = $(this).data('cid');
        chatBox.adjustBoxesOnClose();
        chatBox.close(id);
    });

    /**
     * When a conversation link is clicked show up the respective
     * conversation chatbox
     */

    $('a.conversation').click(function (e) {
        e.preventDefault();

        var conversation_id = $(this).data('cid');
        chatBox.chatWith(conversation_id);
    });


}


chat_typing_intervals = {};

$(document).on('keypress', '.chatboxtextarea', function (ev) {
  var jid = $(this).data('cid') + $('#chat_url').val();

  if (ev.keyCode === 13) {
    ev.preventDefault();
    var body = $(this).val();

    if (body != "") {
      var message = $msg({to: jid,
        "type": "chat"})
      .c('body', {}).t(body).up()
      .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
      conn.send(message);
      var message_content = "<li class='self'>" +
                                "<div class='avatar'>" +
                                    "<img src=" + $("#user_image").val() + ">" +
                                 "</div>" +
                                "<div class='chatboxmessagecontent'>" +
                                    "<p>" + body + "</p>" +
                                    "<time title='" + format_date(new Date()) + "'>" +
                                        "<p>" + $("#firstname").val() + " • " + (format_time(new Date())) +"</p>" +
                                    "</time>" +
                                "</div>" +
                             "</li>";
      $(this).parent().siblings('.chatboxcontent').append(message_content);
      $(this).parent().siblings(".chatboxcontent").scrollTop($(this).parent().siblings('.chatboxcontent')[0].scrollHeight);
      $(this).val('');

      // var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
      //   .c("item", { jid: jid, name: jid.split('@')[0], groups: "buddies"});
      // conn.sendIQ(iq);
      
      var subscribe = $pres({to: jid, "type": "subscribe"});
      conn.send(subscribe);
    }
  } else {
    var notify = $msg({to: jid, "type": "chat"})
    .c('composing', {xmlns: "http://jabber.org/protocol/chatstates"});
    conn.send(notify);
  }
});


Date.prototype.setISO8601 = function(dString){
  var regexp = /(\d\d\d\d)(-)?(\d\d)(-)?(\d\d)(T)?(\d\d)(:)?(\d\d)(:)?(\d\d)(\.\d+)?(Z|([+-])(\d\d)(:)?(\d\d))/;
  if (dString.toString().match(new RegExp(regexp))) {
    var d = dString.match(new RegExp(regexp));
    var offset = 0;
    this.setUTCDate(1);
    this.setUTCFullYear(parseInt(d[1],10));
    this.setUTCMonth(parseInt(d[3],10) - 1);
    this.setUTCDate(parseInt(d[5],10));
    this.setUTCHours(parseInt(d[7],10));
    this.setUTCMinutes(parseInt(d[9],10));
    this.setUTCSeconds(parseInt(d[11],10));
    if (d[12]) {
      this.setUTCMilliseconds(parseFloat(d[12]) * 1000);
    }
    else {
      this.setUTCMilliseconds(0);
    }
    if (d[13] != 'Z') {
      offset = (d[15] * 60) + parseInt(d[17],10);
      offset *= ((d[14] == '-') ? -1 : 1);
      this.setTime(this.getTime() - offset * 60 * 1000);
    }
  }
  else {
    this.setTime(Date.parse(dString));
  }
  return this;
};


connect = function(username) {
  var user_jid = username + $('#chat_url').val();
  stored_jid = localStorage.getItem("jid") ? Strophe.getBareJidFromJid(localStorage.getItem("jid")) : '';
  user_jid = stored_jid == user_jid ? localStorage.getItem("jid") : user_jid;
  conn = new Strophe.Connection("http://"+ $("#ip_url").val() +":7070/http-bind/", {'keepalive': true});
  var password = localStorage.getItem("OrangeStack");
  conn.connect(user_jid, password, function (status) {
    console.log(status);
    if (status === Strophe.Status.CONNECTED) {
      localStorage.setItem("jid", conn.jid);
      conn.addHandler(handle_message, null, "message", "chat");
      conn.send($pres());

       // conn.vcard.init(conn);
       // vcard = "null";
       // conn.vcard.get(function(stanza) {
       //    vcard = $(stanza).find('vCard')[0];
       //    console.log("vcard = ");
       //    console.log(vcard);
       //    data = "<IMAGEURL>" + $('#user_image').val() + "</IMAGEURL>" +
       //           "<NAME>" + $("#firstname").val() + "</NAME>";
       //    conn.vcard.set(function(stanzaa) {
       //    }, $(vcard).html(data)[0], conn.jid.split('/')[0]);
       //  }, conn.jid.split('/')[0]);
      // conn.addHandler(onSubscriptionRequest, null, "presence", "subscribe");
    } else if (status === Strophe.Status.DISCONNECTED) {
      var username = $('#user_id').val();
      connect(username);
    }
  });
}

fetch_user_info = function(message, username_receiver) {
  $.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: "/user_info_for_chat?jid=" + username_receiver,
    dataType: "json",
    success: function (result) {
      process_message(message, username_receiver, result['first_name'], result['image_url']);
    },
    error: function (){
      console.log("Unable to chat :(");
    }
  });
}

process_message = function(message, username_receiver, first_name, user_image) {
  var message_content = "<li class='other'>" +
                          "<div class='avatar'>" +
                              "<img src=" + user_image + ">" +
                          "</div>" +
                          "<div class='chatboxmessagecontent'>" +
                              "<p>" + message + "</p>" +
                              "<time title='" + format_date(new Date()) + "'>" +
                                  "<p>" + first_name + " • " + (format_time(new Date())) +"</p>" +
                              "</time>" +
                          "</div>" +
                      "</li>";
  $('#chatbox_' + username_receiver).css('display', 'block');
  $('#chatbox_' + username_receiver + ' .chatboxinput').css('display', 'block');
  $("#chatbox_" + username_receiver + " .chatboxcontent").css('display', 'block');
  $("#chatbox_" + username_receiver + " .chatboxcontent").append("<p>" + message_content + "</p>");
  $("#chatbox_" + username_receiver + " .chatboxcontent").scrollTop($("#chatbox_" + username_receiver + " .chatboxcontent")[0].scrollHeight);
  chatBox.restructure();
}

handle_message = function(message) {
  var username_receiver = $(message).attr('from').split('@')[0];

  if ($(message).find('composing').length > 0) {
    if ($("#chatbox_"+username_receiver).length > 0) {
      var sender_name = $("#chatbox_" + username_receiver + " .chatboxtitle h1").text();
      $("#chatbox_" + username_receiver + " #is-typing").removeClass('hidden');
      clearTimeout(chat_typing_intervals[username_receiver]);
      chat_typing_intervals[username_receiver] = null;
      chat_typing_intervals[username_receiver] = setTimeout(function() {
        $("#chatbox_" + username_receiver + " #is-typing").addClass('hidden');
      }, 3000);
    }
  } else if ($(message).find('body').length > 0) {
    console.log("message received");
    console.log(message);
    // var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
    //     .c("item", { jid: $(message).attr('from').split('/')[0], name: $(message).attr('from').split('@')[0], groups: "buddies"});
    // conn.sendIQ(iq);
    
    // var subscribe = $pres({to: $(message).attr('from').split('/')[0], "type": "subscribe"});
    // conn.send(subscribe);

    clearTimeout(chat_typing_intervals[username_receiver]);
    $("#chatbox_" + username_receiver + " #is-typing").addClass('hidden');
    if($("#chatbox_"+username_receiver).length > 0) {
      if($("#chatbox_" + username_receiver + " .chatboxcontent .other img").length > 0) {
        var sender_name = $("#chatbox_" + username_receiver + " .chatboxtitle h1").text();
        var sender_image = $("#chatbox_" + username_receiver + " .chatboxcontent .other img").first().attr('src');
        process_message($(message).find('body').text(), username_receiver, sender_name, sender_image);
      } else {
        fetch_user_info($(message).find('body').text(), username_receiver);
      }
    } else {
      notify();
      chatBox.chatWith(username_receiver, $(message).find('body').text());
    }
  }
  return true;
}

var notify = function () {
    var audioplayer = $('#chatAudio')[0];
    audioplayer.play();
}

establish_connection = function(username) {
  connect(username);

  $(window).unload(function() {
    console.log("logged out");
    conn._options.sync = true;    
    conn.send($pres({
      type: "unavailable"
    }));
    conn.disconnect("logged out"); 
    conn.flush();
  });
}

format_date = function(dateObject) {
    var d = new Date(dateObject);
    var minutes = d.getMinutes();
    var hours = d.getHours()
    var day = d.getDate();

    var locale = "en-us";
    var month = d.toLocaleString(locale, {month: "short"});
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + ' ' + month + ' ' + year + ' at ' + hours + ':' + minutes;

    return date;
};

format_time = function(dateObject) {
  var d = new Date(dateObject);
  var minutes = d.getMinutes();
  var hours = d.getHours();

  return  hours + ':' + minutes;
}

$(document).ready(ready);
$(document).on("page:load", ready);

jQuery(function() {
  jQuery('.starbox').each(function() {
    var starbox = jQuery(this);
    starbox.starbox({
      average: starbox.attr('data-start-value'),
      changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
      ghosting: starbox.hasClass('ghosting'),
      autoUpdateAverage: starbox.hasClass('autoupdate'),
      buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
      stars: starbox.attr('data-star-count') || 5
    }).bind('starbox-value-changed', function(event, value) {
      jQuery('#rating-value').val(value);
      if(starbox.hasClass('random')) {
        var val = Math.random();
        starbox.next().text('Random: '+val);
        return val;
      }
    });
  });
});
