(->
  hide_all_search_fields = ->
    $('#q_education_level_eq_div').addClass('hidden')
    $('#q_major_cont_div').addClass('hidden')
    $('#q_teaching_modules_cont_div').addClass('hidden')
    $('#q_job_cont_div').addClass('hidden')
    $('#q_hobbies_cont_div').addClass('hidden')

    $('#q_education_level_in_').prop 'disabled', true
    $('#q_major_cont').prop 'disabled', true
    $('#q_teaching_modules_cont').prop 'disabled', true
    $('#q_job_cont').prop 'disabled', true
    $('#q_hobbies_cont_any_').prop 'disabled', true

  reset_search_fields = ->
    if $('#q_account_type_eq').val() == 'Student'
      hide_all_search_fields()
      $('#q_major_cont_div').removeClass('hidden')
      $('#q_education_level_eq_div').removeClass('hidden')
      $('#q_education_level_in_').prop 'disabled', false
      $('#q_major_cont').prop 'disabled', false

    else if $('#q_account_type_eq').val() == 'Tutor'
      hide_all_search_fields()
      $('#q_teaching_modules_cont_div').removeClass('hidden')
      $('#q_education_level_eq_div').removeClass('hidden')
      $('#q_teaching_modules_cont').prop 'disabled', false
      $('#q_education_level_in_').prop 'disabled', false

    else if $('#q_account_type_eq').val() == 'Employee/Employer'
      hide_all_search_fields()
      $('#q_job_cont_div').removeClass('hidden')
      $('#q_job_cont').prop 'disabled', false

    else if $('#q_account_type_eq').val() == 'Hobby'
      hide_all_search_fields()
      $('#q_hobbies_cont_div').removeClass('hidden')
      $('#q_hobbies_cont_any_').prop 'disabled', false

  bind_user_account_type = ->
    $('#q_account_type_eq').on 'change', ->
      reset_search_fields()

  bind_search_scroll = ->
    search_top = $('.right-side').offset().top

    $(window).scroll ->
      $('.right-side').css({'bottom': 'unset'})

      if $(window).scrollTop() > search_top
        $('.right-side').css({'top': '0px', 'position': 'fixed', 'width': '25%'})
      else
        $('.right-side').css({'top': '0px', 'position': 'static', 'width': '100%'})

      if ($(window).scrollTop() + $(window).height()) > $('footer').offset().top
        $('.right-side').css({'bottom': $('footer').height(), 'top': 'unset'})

  window.Search or (window.Search = {})

  Search.init = ->
    init_controls()

  Search.bind_multiselect = ->
    $('.multiple-selected').each ->
      $(this).multiselect(
        onChange: (option, checked, select) ->
          btn = $('.multiple-selected').next().find('button')
          btn.find('b').remove()
          if $('html').attr('lang') == 'ar' && btn.text().indexOf("selected") >= 0
            numb = btn.text().split(' ')[0]
            btn.text(numb + 'المحدد')
        nonSelectedText: $(this).data('placeholder')
        includeSelectAllOption: true,
        selectAllText: $(this).data('select-all')
        numberDisplayed: 3
      )
      btn_group = $('.multiple-selected').next()
      button = btn_group.find('button')
      button.removeClass 'btn btn-default'
      button.addClass 'form-control form-bg text-left'
      btn_group.addClass 'multi-select-style'
      button.find('b').remove()

  init_controls = ->
    bind_user_account_type()
    reset_search_fields()
    bind_search_scroll()

).call this
