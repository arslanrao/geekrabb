require 'houston'
module PushNotification::PushNotificationHelper
  NOTIFICATION_TYPES = {
    message: 'new_mesage',
    activity: 'activity_created',
    activity_joined: 'activity_joined'
  }

  def send_ios_notification(data, key, token)
    return true if token.blank?
    apn = Houston::Client.development
    file_path = File.join(Rails.root, 'config/certificates', APP_CONFIG[:push_notification][:ios_certificate_file_path])
    apn.certificate = File.read(file_path)

    token.kind_of?(Array) ? handle_multiple_notifications(data, apn, token) : deliver_notification(data, apn, token)
  end

  def handle_multiple_notifications(data, apn, tokens)
    tokens.each do |token|
      deliver_notification(data, apn, token)
    end
  end

  def deliver_notification(data, apn, token)
    notification = Houston::Notification.new(device: token)
    notification.alert = data[:message]
    notification.badge = 0
    notification.sound = :default
    notification.custom_data = data
    apn.push(notification)
  end

  def send_android_notification(data, tokens)
    fcm = FCM.new(APP_CONFIG[:push_notification][:firebase_key])
    response = fcm.send(tokens, data)
  end
end
