class OpenfireClient
  include HTTParty
  
  def initialize()
    @headers = {
      "Authorization" => APP_CONFIG[:openfire][:token],
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }
  end

  def users
    self.class.get("#{APP_CONFIG[:openfire][:rest_api_url]}/users", headers: @headers)
  end

  def create_user user_data
    response = self.class.post("#{APP_CONFIG[:openfire][:rest_api_url]}/users", body: user_data[:openfire].to_json, headers: @headers)
    if response.success?
      self.class.post("#{APP_CONFIG[:openfire][:backend_api_url]}/save_user_info", body: user_data[:vcard_data], headers: {'API-KEY' => APP_CONFIG[:api][:access_token]})
    end
  end

  def update_user user_data
    self.class.put("#{APP_CONFIG[:openfire][:rest_api_url]}/users/#{user_data[:jid]}", body: { "password": user_data[:password]}.to_json, headers: @headers) if user_data[:password].present?

    self.class.post("#{APP_CONFIG[:openfire][:backend_api_url]}/save_user_info", body: user_data[:vcard_data], headers: {'API-KEY' => APP_CONFIG[:api][:access_token]})
  end

  def retrieve_user username
    return if username.blank?

    self.class.get("#{APP_CONFIG[:openfire][:rest_api_url]}/users/#{username}", headers: @headers)
  end

  def destroy_device_token user_identity
    return if user_identity.blank?

    device_type = { "IOS" => 2, "Android" => 1 }[user_identity.device_type]
    return if device_type.blank? || user_identity.device_token.blank?

    jid = user_identity&.user&.jid
    return if jid.blank?

    self.class.post("#{APP_CONFIG[:openfire][:backend_api_url]}/destroy_device_token", body: { device_type: device_type, device_token: user_identity.device_token, jid: jid }, headers: {'API-KEY' => APP_CONFIG[:api][:access_token]})
  end
end
